unit uCalc;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, regexpr, Dialogs,
  Controls, Graphics, StdCtrls, Spin, Menus,
  ExtCtrls, Buttons, ComCtrls, StrUtils, ButtonPanel, ValEdit,
  LCLType, LazHelpHTML, Grids,
  Types, lclintf, MaskEdit, Math, uInfo, fpjson, jsonparser, uTranslationStrings, LCLTranslator;

type
      vektor = array[0..2] of real;

type
      vektorILSW = array[0..2] of string;

type
      punkt = array[0..2] of real;

type
      ebeneobj = object
      private
      public
        stuetzvektor: vektor;
        richtungsvektor1: vektor;
        richtungsvektor2: vektor;
        normalenvektor: vektor;
        normaleneinheitsvektor: vektor;
        normaleneinheitsvektorILSW: vektorILSW;
        koordinatengleich: real;
        koordinatenform: string;
        constructor Create;
        destructor Free;
        procedure genNormalenvektor;
        procedure genNormaleneinheitsvektor;
        procedure genKoordinatenform;
        procedure genNormalenform;
        procedure genParameterform;
        procedure ebeneLaden(jsondaten:string);
        procedure ebeneLadenRegex(jsondaten:string);
        procedure Reset;
        function genJSONdaten():string;
        function punktprobe(p:punkt):boolean;
        function spiegelpunkt(p:punkt):punkt;
        function lotfusspunkt(p:punkt):punkt;
      end;

function kreuzprodukt(v1,v2:vektor):vektor;
function koordinatensumme(v:vektor):real;
function vektorbetrag(v:vektor):real;
function vektorbetragILSW(v:vektor):string;
function vorzeichen(z:real):string;
function hasdecimals(x:real):boolean;
function schnittwinkel(art:string;v1,v2:vektor):string;
function skalarprodukt(v1,v2:vektor):vektor;
function betrag(x:real):real;
function abstand(e:ebeneobj;v,rv:vektor;art:string):string;
function pseudovektor(p:punkt):vektor;
function vektorsubtraktion(v1,v2:vektor):vektor;
function parallelcheck(v1,v2:vektor):boolean;
function orthogonalcheck(v1,v2:vektor):boolean;

implementation

constructor ebeneobj.Create;
begin

end;

destructor ebeneobj.Free;
begin

end;



// Vektoren subtrahieren

function vektorsubtraktion(v1,v2:vektor):vektor;
var erg: vektor;
begin
  erg[0] := v1[0] - v2[0];
  erg[1] := v1[1] - v2[1];
  erg[2] := v1[2] - v2[2];

  result := erg;
end;



// Punkt als Vektor ausgeben

function pseudovektor(p:punkt):vektor;
var v: vektor;
begin
  v[0] := p[0];
  v[1] := p[1];
  v[2] := p[2];

  result := v;
end;



// Abstand Ebene - Punkt

function abstand(e:ebeneobj;v,rv:vektor;art:string):string;
var erg: real;
begin
  if art = 'F' then begin
    if not(parallelcheck(e.normalenvektor,rv)) then begin
      warnung(PChar(i18nNotParallelWarn),PChar(i18nWarning),MB_ICONWARNING);
    end;
  end else if art = 'g' then begin
    if not(orthogonalcheck(e.normalenvektor,rv)) then begin
      warnung(PChar(i18nNotParallelWarn2),PChar(i18nWarning),MB_ICONWARNING);
    end;
  end;

  erg := betrag(koordinatensumme( skalarprodukt( vektorsubtraktion( v,e.stuetzvektor ),e.normaleneinheitsvektor ))); // d(E,P) = |(p - a) * n0|
  result := 'd(E,Q) = ' + FloatToStr(RoundTo(erg,-2)) + ' LE';
end;



// Prüft, ob Vektor 1 orthogonal zu Vektor 2 ist

function orthogonalcheck(v1,v2:vektor):boolean;
begin
  if koordinatensumme(skalarprodukt(v1,v2)) = 0 then begin
    result := true;
  end else result := false;
end;



// Prüft, ob Vektor 1 parallel zu Vektor 2 ist

function parallelcheck(v1,v2:vektor):boolean;
var r1,r2,r3: real;
begin
  if ((v1[0] = 0) and (v1[1] = 0) and (v1[2] = 0)) or
     ((v2[0] = 0) and (v2[1] = 0) and (v2[2] = 0)) then begin
       result := false; // Vektor (0,0,0) ist inakzeptabel, da sein Betrag 0 ist. "False" ist hier die bestmögliche Antwort, auch wenn das eigentlich nicht ganz stimmt.
       exit;
     end;

  if ((v1[0] <> 0) and (v2[0] = 0)) or                      // wenn Vektor 1 ungleich 0 ist, aber Vektor 2 gleich 0, dann können sie nicht parallel sein, weil es kein r gibt,
     ((v1[1] <> 0) and (v2[1] = 0)) or                      // das mit 0 multipliziert etwas anderes als 0 ergibt. Bsp.: Es existiert keine Lösung für 3 = r * 0, weil 3 / 0 = undef.
     ((v1[2] <> 0) and (v2[2] = 0)) then begin
       result := false;
       exit;
     end;

  if (v2[0] <> 0) or (v2[1] <> 0) or (v2[2] <> 0) then begin
       if v2[0] <> 0 then r1 := v1[0] / v2[0];
       if v2[1] <> 0 then r2 := v1[1] / v2[1];
       if v2[2] <> 0 then r3 := v1[2] / v2[2];

       if v2[0] <> 0 then begin
         if v2[1] = 0 then r2 := r1;  // Unten muss ja geprüft werden, ob r1 = r2 = r3. Wenn eine Koordinate von v2 aber 0 ist und somit r alles sein kann (0 = r * 0),
         if v2[2] = 0 then r3 := r1;  // dann sind nur noch die anderen r relevant. Der Einfachheit halber wird das irrelevante r einem der relevanten r gleichgesetzt.
       end;

       if v2[1] <> 0 then begin
         if v2[0] = 0 then r1 := r2;
         if v2[2] = 0 then r3 := r2;
       end;

       if v2[2] <> 0 then begin
         if v2[0] = 0 then r1 := r3;
         if v2[1] = 0 then r2 := r3;
       end;

       if (RoundTo(r1,-2) = RoundTo(r2,-2)) and (RoundTo(r1,-2) = RoundTo(r3,-2)) and (RoundTo(r2,-2) = RoundTo(r3,-2)) then begin
         result := true;
       end else begin
         result := false;
       end;
       exit;
     end;
end;



// Betrag bilden

function betrag(x:real):real;
begin
  if x < 0 then x := x * (-1); // Zahl positiv machen
  result := x;
end;



// Skalarprodukt zweier Vektoren

function skalarprodukt(v1,v2:vektor):vektor;
var erg: vektor;
begin
  erg[0] := v1[0] * v2[0];
  erg[1] := v1[1] * v2[1];
  erg[2] := v1[2] * v2[2];

  result := erg;
end;



// Kreuzprodukt zurueckgeben

function kreuzprodukt(v1,v2:vektor):vektor;
var ergebnis: vektor;
begin
  ergebnis[0] := (v1[1] * v2[2]) - (v1[2] * v2[1]); // Kreuzprodukt halt...
  ergebnis[1] := (v1[2] * v2[0]) - (v1[0] * v2[2]);
  ergebnis[2] := (v1[0] * v2[1]) - (v1[1] * v2[0]);

  result := ergebnis;
end;



// Berechnet den Schnittwinkel zweier Vektoren

function schnittwinkel(art:string;v1,v2:vektor):string;
var winkel: real;
begin
  if art = 'e-e' then begin
    if parallelcheck(v1,v2) = true then begin
      warnung(PChar(i18nParallelWarn),PChar(i18nWarning),MB_ICONWARNING);
    end;
  end else begin
    if parallelcheck(v1,v2) = true then begin
      warnung(PChar(i18nParallelWarn2),PChar(i18nWarning),MB_ICONWARNING);
    end;
  end;

  if art = 'e-e' then begin                                                                                                       // | v1 * v2 |
    winkel := betrag(RadToDeg(arcCos( betrag(koordinatensumme(skalarprodukt(v1,v2))) / (vektorbetrag(v1) * vektorbetrag(v2)) ))); // ----------- = cos(y)
  end else begin                                                                                                                  // |v1| * |v2|
    winkel := betrag(RadToDeg(ArcSin( koordinatensumme(skalarprodukt(v1,v2)) / (vektorbetrag(v1) * vektorbetrag(v2)) )));
  end;

  result := 'ɣ = ' + FloatToStr(RoundTo(winkel,-3)) + '°';
end;



// Prüft, ob Zahl leserlich (ganzzahlig) ist

function hasdecimals(x:real):boolean;
begin
  if (x - trunc(x)) <> 0 then result := true else result := false; // Hat x Nachkommastellen?
end;



// Koordinatensumme eines Vektors zurueckgeben

function koordinatensumme(v:vektor):real;
begin
  result := v[0] + v[1] + v[2];
end;



// Betrag eines Vektors zurueckgeben

function vektorbetrag(v:vektor):real;
begin
  result := sqrt( (v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]) ); // |v| = √(x^2 + y^2 + z^2)
end;



// Betrag eines Vektors In Leserlicher SchreibWeise zurueckgeben

function vektorbetragILSW(v:vektor):string;
var wurzel,zwischenerg: real;
begin
  wurzel := sqrt( (v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]) ); // √(x^2 + y^2 + z^2)

  if hasdecimals(wurzel) then begin
    zwischenerg := RoundTo((v[0] * v[0]) +
                           (v[1] * v[1]) +
                           (v[2] * v[2]),-2);

    if zwischenerg <= 0 then zwischenerg:=zwischenerg * (-1);
    result := '√(' + FloatToStr(zwischenerg) + ')';           // Wurzel nicht ausrechnen, um Leserlichkeit zu erhalten.

  end else result := FloatToStr(wurzel);
end;



// Ebene zurücksetzen

procedure ebeneobj.Reset();
begin
  stuetzvektor[0] := 0;
  stuetzvektor[1] := 0;
  stuetzvektor[2] := 0;

  richtungsvektor1[0] := 0;
  richtungsvektor1[1] := 0;
  richtungsvektor1[2] := 0;

  richtungsvektor2[0] := 0;
  richtungsvektor2[1] := 0;
  richtungsvektor2[2] := 0;

  normalenvektor[0] := 0;
  normalenvektor[1] := 0;
  normalenvektor[2] := 0;

  normaleneinheitsvektor[0] := 0;
  normaleneinheitsvektor[1] := 0;
  normaleneinheitsvektor[2] := 0;

  normaleneinheitsvektorILSW[0] := '';
  normaleneinheitsvektorILSW[1] := '';
  normaleneinheitsvektorILSW[2] := '';

  koordinatengleich := 0;
  koordinatenform := '';
end;



// Ebenendaten aus JSON-String entnehmen

procedure ebeneobj.ebeneLaden(jsondaten:string);
var ebenenJSON: TJSONData;
begin
  ebenenJSON := GetJSON(jsondaten);

  stuetzvektor[0] := StrToFloat(ReplaceText(ebenenJSON.FindPath('stuetzvektor[0]').AsJSON,'"',''));
  stuetzvektor[1] := StrToFloat(ReplaceText(ebenenJSON.FindPath('stuetzvektor[1]').AsJSON,'"',''));
  stuetzvektor[2] := StrToFloat(ReplaceText(ebenenJSON.FindPath('stuetzvektor[2]').AsJSON,'"',''));

  richtungsvektor1[0] := StrToFloat(ReplaceText(ebenenJSON.FindPath('richtungsvektor1[0]').AsJSON,'"',''));
  richtungsvektor1[1] := StrToFloat(ReplaceText(ebenenJSON.FindPath('richtungsvektor1[1]').AsJSON,'"',''));
  richtungsvektor1[2] := StrToFloat(ReplaceText(ebenenJSON.FindPath('richtungsvektor1[2]').AsJSON,'"',''));

  richtungsvektor2[0] := StrToFloat(ReplaceText(ebenenJSON.FindPath('richtungsvektor2[0]').AsJSON,'"',''));
  richtungsvektor2[1] := StrToFloat(ReplaceText(ebenenJSON.FindPath('richtungsvektor2[1]').AsJSON,'"',''));
  richtungsvektor2[2] := StrToFloat(ReplaceText(ebenenJSON.FindPath('richtungsvektor2[2]').AsJSON,'"',''));

  normalenvektor[0] := StrToFloat(ReplaceText(ebenenJSON.FindPath('normalenvektor[0]').AsJSON,'"',''));
  normalenvektor[1] := StrToFloat(ReplaceText(ebenenJSON.FindPath('normalenvektor[1]').AsJSON,'"',''));
  normalenvektor[2] := StrToFloat(ReplaceText(ebenenJSON.FindPath('normalenvektor[2]').AsJSON,'"',''));

  normaleneinheitsvektor[0] := StrToFloat(ReplaceText(ebenenJSON.FindPath('normaleneinheitsvektor[0]').AsJSON,'"',''));
  normaleneinheitsvektor[1] := StrToFloat(ReplaceText(ebenenJSON.FindPath('normaleneinheitsvektor[1]').AsJSON,'"',''));
  normaleneinheitsvektor[2] := StrToFloat(ReplaceText(ebenenJSON.FindPath('normaleneinheitsvektor[2]').AsJSON,'"',''));

  normaleneinheitsvektorILSW[0] := ReplaceText(ebenenJSON.FindPath('normaleneinheitsvektorILSW[0]').AsJSON,'"','');
  normaleneinheitsvektorILSW[1] := ReplaceText(ebenenJSON.FindPath('normaleneinheitsvektorILSW[1]').AsJSON,'"','');
  normaleneinheitsvektorILSW[2] := ReplaceText(ebenenJSON.FindPath('normaleneinheitsvektorILSW[2]').AsJSON,'"','');

  koordinatengleich := StrToFloat(ReplaceText(ebenenJSON.FindPath('koordinatengleich').AsJSON,'"',''));

  koordinatenform := ReplaceText(ebenenJSON.FindPath('koordinatenform').AsJSON,'"','');
end;



// Ebenendaten aus JSON-String entnehmen

procedure ebeneobj.ebeneLadenRegex(jsondaten:string);
begin
  // wird nicht mehr benötigt
end;



// JSON-Daten generieren

function ebeneobj.genJSONdaten():string;  // wird nicht mehr benötigt
var ergebnis: string;
begin
  result := '';
end;



// Normalenvektor generieren

procedure ebeneobj.genNormalenvektor();
begin
  normalenvektor := kreuzprodukt(richtungsvektor1,richtungsvektor2);
end;



// Normaleneinheitsvektor generieren

procedure ebeneobj.genNormaleneinheitsvektor(); // normaleneinheitsvektor = normalenvektor / betrag(normalenvektor)
begin
  normaleneinheitsvektor[0] := RoundTo(normalenvektor[0] / vektorbetrag(normalenvektor),-2);
  normaleneinheitsvektor[1] := RoundTo(normalenvektor[1] / vektorbetrag(normalenvektor),-2);
  normaleneinheitsvektor[2] := RoundTo(normalenvektor[2] / vektorbetrag(normalenvektor),-2);

  normaleneinheitsvektorILSW[0] := FloatToStr(normalenvektor[0]) + ' / ' + vektorbetragILSW(normalenvektor);
  normaleneinheitsvektorILSW[1] := FloatToStr(normalenvektor[1]) + ' / ' + vektorbetragILSW(normalenvektor);
  normaleneinheitsvektorILSW[2] := FloatToStr(normalenvektor[2]) + ' / ' + vektorbetragILSW(normalenvektor);
end;



// Koordinatenform generieren

procedure ebeneobj.genKoordinatenform();
var eq: real;
begin
  eq := (stuetzvektor[0] * normalenvektor[0]) + (stuetzvektor[1] * normalenvektor[1]) + (stuetzvektor[2] * normalenvektor[2]); // -eq = x-Vektor * Normalenvektor
  if eq < 0 then eq := eq * (-1); // eq auf andere Seite bringen

  koordinatenform := FloatToStr(eq) + '=' + FloatToStr(normalenvektor[0]) + 'x' + vorzeichen(normalenvektor[1]) + 'y' + vorzeichen(normalenvektor[2]) + 'z';
end;



// Normalenform generieren

procedure ebeneobj.genNormalenform();
var g,a,b,c:real;
begin
  g := StrToFloat(ReplaceRegExpr('([+-]?)(\d*)=.+',koordinatenform,'$2',true)); // Zahl auf der linken Seite
  if ReplaceRegExpr('([+-]?)(\d*)=.+',koordinatenform,'$1',true) = '-' then g := g * (-1); // falls Minus vor der Zahl, dann negativ machen (weil Minus ja nicht als Minus geparst wird)

  a := StrToFloat(ReplaceRegExpr('.+=([+-]?)([\d,.]+)x([+-]{1})([\d,.]+)y([+-]{1})([\d,.]+)z',koordinatenform,'$2',true)); // Zahl vor dem x
  b := StrToFloat(ReplaceRegExpr('.+=([+-]?)([\d,.]+)x([+-]{1})([\d,.]+)y([+-]{1})([\d,.]+)z',koordinatenform,'$4',true)); //              y
  c := StrToFloat(ReplaceRegExpr('.+=([+-]?)([\d,.]+)x([+-]{1})([\d,.]+)y([+-]{1})([\d,.]+)z',koordinatenform,'$6',true)); //              z

  if ReplaceRegExpr('.+=([+-]?)([\d,.]+)x([+-]{1})([\d,.]+)y([+-]{1})([\d,.]+)z',koordinatenform,'$1',true) = '-' then a := a * (-1); // Vorzeichen vom x -> bei Minus Zahl negativ machen
  if ReplaceRegExpr('.+=([+-]?)([\d,.]+)x([+-]{1})([\d,.]+)y([+-]{1})([\d,.]+)z',koordinatenform,'$3',true) = '-' then b := b * (-1); //                y
  if ReplaceRegExpr('.+=([+-]?)([\d,.]+)x([+-]{1})([\d,.]+)y([+-]{1})([\d,.]+)z',koordinatenform,'$5',true) = '-' then c := c * (-1); //                z

  if a <> 0 then begin
    stuetzvektor[0] := g / a;
    stuetzvektor[1] := 0;
    stuetzvektor[2] := 0;

  end else if b <> 0 then begin // falls a = 0, um Division durch 0 zu vermeiden
    stuetzvektor[0] := 0;
    stuetzvektor[1] := g / b;
    stuetzvektor[2] := 0;

  end else if c <> 0 then begin // falls b = 0... (s.o.)
    stuetzvektor[0] := 0;
    stuetzvektor[1] := 0;
    stuetzvektor[2] := g / c;
  end;
end;



// Parameterform generieren

procedure ebeneobj.genParameterform(); // Bedingung: Richtungsvektor * Normalenvektor muss gleich 0 sein
begin
  richtungsvektor1[0] := normalenvektor[1];             //   x        y
  richtungsvektor1[1] := normalenvektor[0] * (-1);      // ( y ) * ( -x ) = 0
  richtungsvektor1[2] := 0;                             //   z        0

  richtungsvektor2[0] := 0;                             //   x        0
  richtungsvektor2[1] := normalenvektor[2];             // ( y ) * (  z ) = 0
  richtungsvektor2[2] := normalenvektor[1] * (-1);      //   z       -y
end;



// Punktprobe durchfuehren und true/false zurueckgeben

function ebeneobj.punktprobe(p:punkt):boolean;
var g,a,b,c,eq: real;
begin
  g := StrToFloat(ReplaceRegExpr('([+-]?)(\d*)=.+',koordinatenform,'$2',true)); // Zahl auf der linken Seite
  a := StrToFloat(ReplaceRegExpr('.+=([+-]?)(\d+)x([+-]{1})(\d+)y([+-]{1})(\d+)z',koordinatenform,'$2',true)); // Zahl vor dem x
  b := StrToFloat(ReplaceRegExpr('.+=([+-]?)(\d+)x([+-]{1})(\d+)y([+-]{1})(\d+)z',koordinatenform,'$4',true)); //              y
  c := StrToFloat(ReplaceRegExpr('.+=([+-]?)(\d+)x([+-]{1})(\d+)y([+-]{1})(\d+)z',koordinatenform,'$6',true)); //              z

  if ReplaceRegExpr('([+-]?)(\d*)=.+',koordinatenform,'$1',true) = '-' then g := g * (-1); // falls ein Minus vor der Zahl steht, Real-Zahl negativ machen

  if ReplaceRegExpr('.+=([+-]?)(\d+)x([+-]{1})(\d+)y([+-]{1})(\d+)z',koordinatenform,'$1',true) = '-' then a := a * (-1);
  if ReplaceRegExpr('.+=([+-]?)(\d+)x([+-]{1})(\d+)y([+-]{1})(\d+)z',koordinatenform,'$3',true) = '-' then b := b * (-1);
  if ReplaceRegExpr('.+=([+-]?)(\d+)x([+-]{1})(\d+)y([+-]{1})(\d+)z',koordinatenform,'$5',true) = '-' then c := c * (-1);

  eq := (a * p[0]) + (b * p[1]) + (c * p[2]);

  if eq = g then result := true else result := false;
end;



// Spiegelpunkt bestimmen

function ebeneobj.spiegelpunkt(p:punkt):punkt;
var lfp,ergebnis: punkt; eq: string;
begin
   lfp := lotfusspunkt(p);

  ergebnis[0] := RoundTo(p[0] + 2 * (lfp[0] - p[0]), -2); // P' = P + 2 * (LFP - P)
  ergebnis[1] := RoundTo(p[1] + 2 * (lfp[1] - p[1]), -2);
  ergebnis[2] := RoundTo(p[2] + 2 * (lfp[2] - p[2]), -2);

  result := ergebnis;
end;



// Lotfusspunkt bestimmen

function ebeneobj.lotfusspunkt(p:punkt):punkt;
var lfp: punkt; r: real;
begin
  r := ((
       ((p[0] - stuetzvektor[0]) * normalenvektor[0]) +
       ((p[1] - stuetzvektor[1]) * normalenvektor[1]) +
       ((p[2] - stuetzvektor[2]) * normalenvektor[2]))
       * (-1)) / (
       (normalenvektor[0] * normalenvektor[0]) +
       (normalenvektor[1] * normalenvektor[1]) +
       (normalenvektor[2] * normalenvektor[2]));

  lfp[0] := RoundTo(p[0] + r * normalenvektor[0], -2); //                 (P - s) * n
  lfp[1] := RoundTo(p[1] + r * normalenvektor[1], -2); // x(LFP) = x(P) - ----------- * x(n)
  lfp[2] := RoundTo(p[2] + r * normalenvektor[2], -2); //                     n^2

  result := lfp;
end;



// + vor Zahl schreiben, falls positiv

function vorzeichen(z:real):string;
begin
  if z >= 0 then result := '+' + FloatToStr(z) else result := FloatToStr(z); // wenn die Zahl negativ ist, braucht man kein zusätzliches Minus (bei einer positiven Zahl aber ein Plus)
end;

end.

