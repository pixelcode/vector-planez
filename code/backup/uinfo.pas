unit uInfo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, lclintf,
  Spin, Menus, Buttons, ComCtrls, StrUtils, ButtonPanel, ValEdit, LCLType, Grids,
  Types, MaskEdit;

type

  { TInfoForm }

  TInfoForm = class(TForm)
    ImgLogoBadge: TImage;
    Info: TStaticText;
    Info1: TStaticText;
    StaticText1: TStaticText;
    StaticTxtFatcow: TStaticText;
    StaticTxtInnoSetup: TStaticText;
    StaticTxtQuellcode2: TStaticText;
    StaticTxtQuellcode3: TStaticText;
    StaticTxtQuellcode4: TStaticText;
    StaticTxtQuellcodeLink: TStaticText;
    StaticTxtQuellcode: TStaticText;
    StaticTxtLizenzLink: TStaticText;
    StaticTxtLizenz: TStaticText;
    StaticTxtentwicklerLink: TStaticText;
    StaticTxtFPCLink: TStaticText;
    procedure StaticTxtFatcowClick(Sender: TObject);
    procedure StaticTxtInnoSetupClick(Sender: TObject);
    procedure StaticTxtFPCLinkClick(Sender: TObject);
    procedure StaticTxtLizenzLinkClick(Sender: TObject);
    procedure StaticTxtentwicklerLinkClick(Sender: TObject);
    procedure StaticTxtProgrSprPascalLinkClick(Sender: TObject);
    procedure StaticTxtQuellcodeLinkClick(Sender: TObject);
  private

  public

  end;

var
  InfoForm: TInfoForm;

procedure warnung(text,titel:PChar;icon: Longint = MB_OK);

implementation

{$R *.lfm}

{ TInfoForm }

procedure warnung(text,titel:PChar;icon: Longint = MB_OK);
begin
  Application.MessageBox(text,titel,icon); // ...damit die Unit uCalc auch ohne eigenes TForm Icon-Dialoge ausgeben kann
end;                                       // Nicht die allerbeste Programmierpraxis, aber einfache, verständliche statt unnötig komplizierter Lösung.

procedure TInfoForm.StaticTxtentwicklerLinkClick(Sender: TObject);
begin
  OpenURL('https://codeberg.org/pixelcode');
end;

procedure TInfoForm.StaticTxtQuellcodeLinkClick(Sender: TObject);
begin
  OpenURL('https://codeberg.org/pixelcode/vector-planez');
end;

procedure TInfoForm.StaticTxtLizenzLinkClick(Sender: TObject);
begin
  OpenURL('https://codeberg.org/pixelcode/index/src/branch/master/PIXELCODE-LICENCE.md');
end;

procedure TInfoForm.StaticTxtFPCLinkClick(Sender: TObject);
begin
  OpenURL('https://www.freepascal.org');
end;

procedure TInfoForm.StaticTxtFatcowClick(Sender: TObject);
begin
  OpenURL('https://fatcow.com/free-icons');
end;

procedure TInfoForm.StaticTxtInnoSetupClick(Sender: TObject);
begin
 OpenURL('https://jrsoftware.org/isinfo.php');
end;

end.

