unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Spin, Menus,
  ExtCtrls, Buttons, ComCtrls, uCalc, uInfo, StrUtils, ButtonPanel, ValEdit,
  LCLType, LazHelpHTML, Grids, Types, lclintf, MaskEdit, JSONPropStorage,
  FileAssoc, CheckBoxThemed, regexpr, DateUtils, fpjson, jsonparser, uTranslationStrings, LCLTranslator;

type

  { TMainForm }

  TMainForm = class(TForm)
    BitBtnCheckPunkt: TBitBtn;
    BitBtnVektorCheckOK: TBitBtn;
    BitBtnWinkelOK: TBitBtn;
    BitBtnKreuzproduktOK: TBitBtn;
    BitBtnOrthGeradeOK: TBitBtn;
    BitBtnOK: TBitBtn;
    BitBtnAbstandOK: TBitBtn;
    BtnOrthSV: TButton;
    BtnSekEbeneSpeichern: TButton;
    ComboBoxCheckVektor: TComboBox;
    FileAssociation1: TFileAssociation;
    FltSpnEdtGeradeRVx1: TFloatSpinEdit;
    FltSpnEdtGeradeRVy1: TFloatSpinEdit;
    FltSpnEdtGeradeRVz1: TFloatSpinEdit;
    FltSpnEdtGeradeSVx1: TFloatSpinEdit;
    FltSpnEdtGeradeSVy1: TFloatSpinEdit;
    FltSpnEdtGeradeSVz1: TFloatSpinEdit;
    FltSpnEdtKoordinatenG: TFloatSpinEdit;
    FltSpnEdtKoordX: TFloatSpinEdit;
    FltSpnEdtKoordY: TFloatSpinEdit;
    FltSpnEdtKoordZ: TFloatSpinEdit;
    FltSpnEdtCheckVektorX1: TFloatSpinEdit;
    FltSpnEdtCheckVektorY1: TFloatSpinEdit;
    FltSpnEdtCheckVektorZ1: TFloatSpinEdit;
    FltSpnEdtCheckVektorX2: TFloatSpinEdit;
    FltSpnEdtCheckVektorY2: TFloatSpinEdit;
    FltSpnEdtCheckVektorZ2: TFloatSpinEdit;
    FltSpnEdtPunktX: TFloatSpinEdit;
    FltSpnEdtPunktX1: TFloatSpinEdit;
    FltSpnEdtPunktY1: TFloatSpinEdit;
    FltSpnEdtPunktZ1: TFloatSpinEdit;
    FltSpnEdtRVx1: TFloatSpinEdit;
    FltSpnEdtRVx2: TFloatSpinEdit;
    FltSpnEdtGeradeRVx: TFloatSpinEdit;
    FltSpnEdtRVy1: TFloatSpinEdit;
    FltSpnEdtRVy2: TFloatSpinEdit;
    FltSpnEdtGeradeRVy: TFloatSpinEdit;
    FltSpnEdtRVz1: TFloatSpinEdit;
    FltSpnEdtRVz2: TFloatSpinEdit;
    FltSpnEdtGeradeRVz: TFloatSpinEdit;
    FltSpnEdtSPunktX: TFloatSpinEdit;
    FltSpnEdtPunktY: TFloatSpinEdit;
    FltSpnEdtSPunktY: TFloatSpinEdit;
    FltSpnEdtPunktZ: TFloatSpinEdit;
    FltSpnEdtSPunktZ: TFloatSpinEdit;
    FltSpnEdtSVx: TFloatSpinEdit;
    FltSpnEdtNormalenSVx: TFloatSpinEdit;
    FltSpnEdtNormalenNVx: TFloatSpinEdit;
    FltSpnEdtKreuzproduktVektor1X: TFloatSpinEdit;
    FltSpnEdtKreuzproduktVektor1Y: TFloatSpinEdit;
    FltSpnEdtKreuzproduktVektor1Z: TFloatSpinEdit;
    FltSpnEdtKreuzproduktVektor2X: TFloatSpinEdit;
    FltSpnEdtKreuzproduktVektor2Y: TFloatSpinEdit;
    FltSpnEdtKreuzproduktVektor2Z: TFloatSpinEdit;
    FltSpnEdtGeradeSVx: TFloatSpinEdit;
    FltSpnEdtSVy: TFloatSpinEdit;
    FltSpnEdtNormalenSVy: TFloatSpinEdit;
    FltSpnEdtNormalenNVy: TFloatSpinEdit;
    FltSpnEdtGeradeSVy: TFloatSpinEdit;
    FltSpnEdtSVz: TFloatSpinEdit;
    FltSpnEdtNormalenSVz: TFloatSpinEdit;
    FltSpnEdtNormalenNVz: TFloatSpinEdit;
    FltSpnEdtGeradeSVz: TFloatSpinEdit;
    ImgPunktprobeErgIcon: TImage;
    ImgPunktprobeErgIcon1: TImage;
    ImgPunktprobeErgIcon2: TImage;
    ImgPunktprobeErgIcon3: TImage;
    ImgPunktprobeErgIcon4: TImage;
    ImgPunktprobeErgIcon5: TImage;
    ImgPunktprobeErgIcon6: TImage;
    ImgVektorcheckErgIcon: TImage;
    JSONPropStorage1: TJSONPropStorage;
    Label1: TLabel;
    Label2: TLabel;
    MainMenuVP: TMainMenu;
    MaskEdit1: TMaskEdit;
    MenuDatei: TMenuItem;
    MenuHilfe: TMenuItem;
    MenuDateiOeffnen: TMenuItem;
    MenuDateiSpeichern: TMenuItem;
    MenuDoku: TMenuItem;
    MenuBugReport: TMenuItem;
    MenuBearbeiten: TMenuItem;
    MenuSprache: TMenuItem;
    MenuDeutsch: TMenuItem;
    MenuEnglisch: TMenuItem;
    MenuResetAll: TMenuItem;
    MenuUeber: TMenuItem;
    OpenDlgMain: TOpenDialog;
    PgCtrlEbenenEingabe: TPageControl;
    PgCtrlAktionen: TPageControl;
    RadioBtnWinkelF: TRadioButton;
    RadioBtnAbstandEF: TRadioButton;
    RadioBtnWinkelg: TRadioButton;
    RadioBtnAbstandEg: TRadioButton;
    RadioBtnAbstandEP: TRadioButton;
    RadioGroup1: TRadioGroup;
    RadioGroupAbstandE: TRadioGroup;
    SaveDlgMain: TSaveDialog;
    StaticText1: TStaticText;
    StaticText10: TStaticText;
    StaticText11: TStaticText;
    StaticText12: TStaticText;
    StaticText13: TStaticText;
    StaticText14: TStaticText;
    StaticText15: TStaticText;
    StaticText16: TStaticText;
    StaticText17: TStaticText;
    StaticText18: TStaticText;
    StaticText19: TStaticText;
    StaticText2: TStaticText;
    StaticText20: TStaticText;
    StaticText21: TStaticText;
    StaticText22: TStaticText;
    StaticText23: TStaticText;
    StaticText24: TStaticText;
    StaticText25: TStaticText;
    StaticText26: TStaticText;
    StaticText27: TStaticText;
    StaticText28: TStaticText;
    StaticText29: TStaticText;
    StaticText3: TStaticText;
    StaticText30: TStaticText;
    StaticText31: TStaticText;
    StaticText32: TStaticText;
    StaticText33: TStaticText;
    StaticText34: TStaticText;
    StaticText35: TStaticText;
    StaticText36: TStaticText;
    StaticText37: TStaticText;
    StaticText38: TStaticText;
    StaticText39: TStaticText;
    StaticText40: TStaticText;
    StaticText41: TStaticText;
    StaticText42: TStaticText;
    StaticText43: TStaticText;
    StaticText44: TStaticText;
    StaticText45: TStaticText;
    StaticText46: TStaticText;
    StaticText47: TStaticText;
    StaticText48: TStaticText;
    StaticText49: TStaticText;
    StaticText50: TStaticText;
    StaticText51: TStaticText;
    StaticText52: TStaticText;
    StaticText53: TStaticText;
    StaticText54: TStaticText;
    StaticText55: TStaticText;
    StaticText56: TStaticText;
    StaticText57: TStaticText;
    StaticText58: TStaticText;
    StaticText59: TStaticText;
    StaticText60: TStaticText;
    StaticText61: TStaticText;
    StaticText62: TStaticText;
    StaticText63: TStaticText;
    StaticText64: TStaticText;
    StaticText65: TStaticText;
    StaticText66: TStaticText;
    StaticText67: TStaticText;
    StaticTxtE17: TStaticText;
    StaticTxtErgebnisVektorCheck1: TStaticText;
    StaticTxtGrauWarn1: TStaticText;
    StaticTxtGrauWarn2: TStaticText;
    StaticTxtKreuzproduktHeadline1: TStaticText;
    StaticTxtParamformSVx: TStaticText;
    StaticTxtParamformSVy: TStaticText;
    StaticTxtParamformSVz: TStaticText;
    StaticTxtPunkt14: TStaticText;
    StaticTxtPunkt15: TStaticText;
    StaticTxtPunkt17: TStaticText;
    StaticTxtPunktprobeBeschr3: TStaticText;
    StaticTxtR4: TStaticText;
    StaticTxtWinkel: TStaticText;
    StaticTxtE1: TStaticText;
    StaticTxtE16: TStaticText;
    StaticTxtKreuzproduktHeadline: TStaticText;
    StaticTxtGleichungsform: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    StaticText9: TStaticText;
    StaticTxOrthGerRVx: TStaticText;
    StaticTxOrthGerRVy: TStaticText;
    StaticTxOrthGerRVz: TStaticText;
    StaticTxOrthGerSPx: TStaticText;
    StaticTxOrthGerSPy: TStaticText;
    StaticTxOrthGerSPz: TStaticText;
    StaticTxtE: TStaticText;
    StaticTxtE10: TStaticText;
    StaticTxtE11: TStaticText;
    StaticTxtE12: TStaticText;
    StaticTxtE13: TStaticText;
    StaticTxtE14: TStaticText;
    StaticTxtE15: TStaticText;
    StaticTxtE2: TStaticText;
    StaticTxtE3: TStaticText;
    StaticTxtE4: TStaticText;
    StaticTxtE5: TStaticText;
    StaticTxtE6: TStaticText;
    StaticTxtE7: TStaticText;
    StaticTxtE8: TStaticText;
    StaticTxtE9: TStaticText;
    StaticTxtGleichungsform1: TStaticText;
    StaticTxtGleichungsform2: TStaticText;
    StaticTxtGrauWarn: TStaticText;
    StaticTxtGrauWarnOrth: TStaticText;
    StaticTxtKoordinatenform: TStaticText;
    StaticTxtNormalenformNormalvektorX: TStaticText;
    StaticTxtNormalenformNormalvektorY: TStaticText;
    StaticTxtNormalenformNormalvektorZ: TStaticText;
    StaticTxtNormalenformSVx: TStaticText;
    StaticTxtNormalenformSVy: TStaticText;
    StaticTxtNormalenformSVz: TStaticText;
    StaticTxtNormalenvektorX: TStaticText;
    StaticTxtNormalenvektorY: TStaticText;
    StaticTxtNormalenvektorZ: TStaticText;
    StaticTxtParamformRV1x: TStaticText;
    StaticTxtKreuzproduktX: TStaticText;
    StaticTxtParamformRV1y: TStaticText;
    StaticTxtKreuzproduktY: TStaticText;
    StaticTxtParamformRV1z: TStaticText;
    StaticTxtKreuzproduktZ: TStaticText;
    StaticTxtParamformRV2x: TStaticText;
    StaticTxtParamformRV2y: TStaticText;
    StaticTxtParamformRV2z: TStaticText;
    StaticTxtPunkt10: TStaticText;
    StaticTxtPunkt11: TStaticText;
    StaticTxtPunkt12: TStaticText;
    StaticTxtPunktprobeBeschr2: TStaticText;
    StaticTxtR2: TStaticText;
    StaticTxtR3: TStaticText;
    StaticTxtS1: TStaticText;
    StaticTxtSpiegelPunkt: TStaticText;
    StaticTxtPunkt6: TStaticText;
    StaticTxtPunkt7: TStaticText;
    StaticTxtPunkt8: TStaticText;
    StaticTxtPunkt9: TStaticText;
    StaticTxtPunktprobeBeschr1: TStaticText;
    StaticTxtR: TStaticText;
    StaticTxtR1: TStaticText;
    StaticTxtPunktprobeBeschr: TStaticText;
    StaticTxtPunkt: TStaticText;
    StaticTxtPunkt1: TStaticText;
    StaticTxtPunkt2: TStaticText;
    StaticTxtPunkt3: TStaticText;
    StaticTxtPunkt4: TStaticText;
    StaticTxtPunkt5: TStaticText;
    StaticTxtErgebnisPunktcheck: TStaticText;
    StaticTxtS: TStaticText;
    StaticTxtLotfusspunkt: TStaticText;
    StaticTxtAbstand: TStaticText;
    TabCtrlNormalenVektor: TTabControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheetAbstand: TTabSheet;
    TabSheetWerkzeuge: TTabSheet;
    TabSheetUmformung: TTabSheet;
    TabSheetKoordinatenform: TTabSheet;
    TabSheetNormalenform: TTabSheet;
    TabSheetEingabeParamForm: TTabSheet;
    TabSheetPktProbe: TTabSheet;
    TabSheetStd: TTabSheet;
    TaskDlgMain: TTaskDialog;
    procedure BitBtnAbstandOKClick(Sender: TObject);
    procedure BitBtnVektorCheckOKClick(Sender: TObject);
    procedure BitBtnWinkelOKClick(Sender: TObject);
    procedure BitBtnCheckPunktClick(Sender: TObject);
    procedure BitBtnKreuzproduktOKClick(Sender: TObject);
    procedure BitBtnOKClick(Sender: TObject);
    procedure BitBtnOrthGeradeOKClick(Sender: TObject);
    procedure BtnDateiOeffnenClick(Sender: TObject);
    procedure BtnOrthSVClick(Sender: TObject);
    procedure BtnSekEbeneSpeichernClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuBugReportClick(Sender: TObject);
    procedure MenuDateiOeffnenClick(Sender: TObject);
    procedure MenuDateiSpeichernClick(Sender: TObject);
    procedure MenuDeutschClick(Sender: TObject);
    procedure MenuDokuClick(Sender: TObject);
    procedure MenuEnglischClick(Sender: TObject);
    procedure MenuResetAllClick(Sender: TObject);
    procedure MenuUeberClick(Sender: TObject);
    procedure TabCtrlNormalenVektorChange(Sender: TObject);
  private
    assoc: TFileAssociation;
  public

  end;

var
  MainForm: TMainForm;
  ebene,sekundaerebene: ebeneobj;
  checkpunkt: punkt;
  vectorplanezdirectory: string;

procedure ebeneSpeichernRest();
procedure ebeneSonstDatenLaden(jsondaten:string);
procedure WerteZuruecksetzen();
procedure eingrauen();
procedure fehlermeldungDateifehler(fehler:string);
procedure spracheAendern(sprache:string);
function genSonstJSONdaten():string;

implementation

{$R *.lfm}

{ TMainForm }



// Deaktivierte Felder aktivieren
// (da benötigte Daten durch den Nutzer eingegeben wurden)

procedure entgrauen();
begin
  MainForm.FltSpnEdtPunktX.Enabled := True;
  MainForm.FltSpnEdtPunktY.Enabled := True;
  MainForm.FltSpnEdtPunktZ.Enabled := True;

  MainForm.FltSpnEdtSPunktX.Enabled := True;
  MainForm.FltSpnEdtSPunktY.Enabled := True;
  MainForm.FltSpnEdtSPunktZ.Enabled := True;

  MainForm.BitBtnCheckPunkt.Enabled := True;
  MainForm.BitBtnOrthGeradeOK.Enabled := True;
  MainForm.BtnOrthSV.Enabled := True;

  MainForm.StaticTxtGrauWarn.Caption := '';
  MainForm.StaticTxtGrauWarnOrth.Caption := '';

  MainForm.BitBtnWinkelOK.Enabled := True;
  MainForm.BitBtnAbstandOK.Enabled := True;
  MainForm.StaticTxtGrauWarn1.Caption := '';
  MainForm.StaticTxtGrauWarn2.Caption := '';
end;



// Deaktivierte, die Sekundärebene F betreffende Felder aktivieren

procedure entgrauenF();
begin
  MainForm.RadioBtnWinkelF.Enabled := True;
  MainForm.RadioBtnAbstandEF.Enabled := True;

  if MainForm.StaticTxtGrauWarn1.Caption <> '' then MainForm.StaticTxtGrauWarn1.Caption := i18nSavePlaneFirst; // Hab leider vergessen, was das hier soll... :(
  if MainForm.StaticTxtGrauWarn2.Caption <> '' then MainForm.StaticTxtGrauWarn2.Caption := i18nSavePlaneFirst;
end;



// alles eingrauen (bei Reset)

procedure eingrauen();
begin
  MainForm.FltSpnEdtPunktX.Enabled := False;
  MainForm.FltSpnEdtPunktY.Enabled := False;
  MainForm.FltSpnEdtPunktZ.Enabled := False;

  MainForm.FltSpnEdtSPunktX.Enabled := False;
  MainForm.FltSpnEdtSPunktY.Enabled := False;
  MainForm.FltSpnEdtSPunktZ.Enabled := False;

  MainForm.BitBtnCheckPunkt.Enabled := False;
  MainForm.BitBtnOrthGeradeOK.Enabled := False;
  MainForm.BtnOrthSV.Enabled := False;

  MainForm.StaticTxtGrauWarn.Caption := i18nSavePlaneFirst;
  MainForm.StaticTxtGrauWarnOrth.Caption := i18nSavePlaneFirst;

  MainForm.BitBtnWinkelOK.Enabled := False;
  MainForm.BitBtnAbstandOK.Enabled := False;
  MainForm.StaticTxtGrauWarn1.Caption := i18nSavePlaneFirst2;
  MainForm.StaticTxtGrauWarn2.Caption := i18nSavePlaneFirst2;

  MainForm.RadioBtnWinkelF.Enabled := False;
  MainForm.RadioBtnAbstandEF.Enabled := False;
end;



// Eingegebene Ebene speichern und Gleichung umformen etc.

procedure TMainForm.BitBtnOKClick(Sender: TObject);
var testnv1,testnv2: vektor;
begin
  ebene.Create;

  case PgCtrlEbenenEingabe.TabIndex of
    0: begin // falls Parameterform
      testnv1[0] := FltSpnEdtRVx1.Value;
      testnv1[1] := FltSpnEdtRVy1.Value;
      testnv1[2] := FltSpnEdtRVz1.Value;

      testnv2[0] := FltSpnEdtRVx2.Value;
      testnv2[1] := FltSpnEdtRVy2.Value;
      testnv2[2] := FltSpnEdtRVz2.Value;

      if (kreuzprodukt(testnv1,testnv2)[0] = 0) and (kreuzprodukt(testnv1,testnv2)[1] = 0) and (kreuzprodukt(testnv1,testnv2)[2] = 0) then begin
        Application.MessageBox(PChar(i18nZerovectorwarn),PChar(i18nError),MB_ICONERROR);
        exit;
      end else begin
        ebene.stuetzvektor[0] := FltSpnEdtSVx.Value;
        ebene.stuetzvektor[1] := FltSpnEdtSVy.Value;
        ebene.stuetzvektor[2] := FltSpnEdtSVz.Value;

        ebene.richtungsvektor1[0] := FltSpnEdtRVx1.Value;
        ebene.richtungsvektor1[1] := FltSpnEdtRVy1.Value;
        ebene.richtungsvektor1[2] := FltSpnEdtRVz1.Value;

        ebene.richtungsvektor2[0] := FltSpnEdtRVx2.Value;
        ebene.richtungsvektor2[1] := FltSpnEdtRVy2.Value;
        ebene.richtungsvektor2[2] := FltSpnEdtRVz2.Value;

        ebene.genNormalenvektor();
        ebene.genKoordinatenform();
      end;
    end;

    1: begin // falls Normalenform
      if (FltSpnEdtNormalenNVx.Value = 0) and (FltSpnEdtNormalenNVy.Value = 0) and (FltSpnEdtNormalenNVz.Value = 0) then begin
        Application.MessageBox(PChar(i18nZerovectorwarn),PChar(i18nError),MB_ICONERROR);
        exit;
      end else begin
        ebene.stuetzvektor[0] := FltSpnEdtNormalenSVx.Value;
        ebene.stuetzvektor[1] := FltSpnEdtNormalenSVy.Value;
        ebene.stuetzvektor[2] := FltSpnEdtNormalenSVz.Value;

        ebene.normalenvektor[0] := FltSpnEdtNormalenNVx.Value;
        ebene.normalenvektor[1] := FltSpnEdtNormalenNVy.Value;
        ebene.normalenvektor[2] := FltSpnEdtNormalenNVz.Value;

        ebene.genKoordinatenform();
        ebene.genParameterform();
      end;

    end;

    2: begin // falls Koordinatenform
      if (FltSpnEdtKoordX.Value = 0) and (FltSpnEdtKoordY.Value = 0) and (FltSpnEdtKoordZ.Value = 0) then begin
        Application.MessageBox(PChar(i18nZerovectorwarn),PChar(i18nError),MB_ICONERROR);
        exit;
      end else begin
        ebene.normalenvektor[0] := FltSpnEdtKoordX.Value;
        ebene.normalenvektor[1] := FltSpnEdtKoordY.Value;
        ebene.normalenvektor[2] := FltSpnEdtKoordZ.Value;

        ebene.koordinatenform := FloatToStr(FltSpnEdtKoordinatenG.Value) + '=' +
                                 FloatToStr(ebene.normalenvektor[0]) + 'x' +
                                 vorzeichen(ebene.normalenvektor[1]) + 'y' +
                                 vorzeichen(ebene.normalenvektor[2]) + 'z';

        ebene.genNormalenform();
        ebene.genParameterform();
      end;
    end;
  end;

  ebeneSpeichernRest();

  Application.MessageBox(PChar(i18nPlaneSaved),PChar(i18nSuccess),MB_ICONINFORMATION);

  ebene.free;
end;



// Speichern der Ebene

procedure ebeneSpeichernRest();
begin
  ebene.genNormaleneinheitsvektor();

  MainForm.StaticTxtParamformSVx.Caption := FloatToStr(ebene.stuetzvektor[0]);
  MainForm.StaticTxtParamformSVy.Caption := FloatToStr(ebene.stuetzvektor[1]);
  MainForm.StatictxtParamformSVz.Caption := FloatToStr(ebene.stuetzvektor[2]);

  MainForm.StaticTxtParamformRV1x.Caption := FloatToStr(ebene.richtungsvektor1[0]);
  MainForm.StaticTxtParamformRV1y.Caption := FloatToStr(ebene.richtungsvektor1[1]);
  MainForm.StaticTxtParamformRV1z.Caption := FloatToStr(ebene.richtungsvektor1[2]);

  MainForm.StaticTxtParamformRV2x.Caption := FloatToStr(ebene.richtungsvektor2[0]);
  MainForm.StaticTxtParamformRV2y.Caption := FloatToStr(ebene.richtungsvektor2[1]);
  MainForm.StaticTxtParamformRV2z.Caption := FloatToStr(ebene.richtungsvektor2[2]);

  MainForm.StaticTxtNormalenformSVx.Caption := FloatToStr(ebene.stuetzvektor[0]);
  MainForm.StaticTxtNormalenformSVy.Caption := FloatToStr(ebene.stuetzvektor[1]);
  MainForm.StaticTxtNormalenformSVz.Caption := FloatToStr(ebene.stuetzvektor[2]);

  MainForm.StaticTxtNormalenformNormalvektorX.Caption := FloatToStr(ebene.normalenvektor[0]);
  MainForm.StaticTxtNormalenformNormalvektorY.Caption := FloatToStr(ebene.normalenvektor[1]);
  MainForm.StaticTxtNormalenformNormalvektorZ.Caption := FloatToStr(ebene.normalenvektor[2]);

  MainForm.StaticTxtKoordinatenform.Caption := ebene.koordinatenform;

  MainForm.StaticTxtNormalenvektorX.Caption := FloatToStr(ebene.normalenvektor[0]);
  MainForm.StaticTxtNormalenvektorY.Caption := FloatToStr(ebene.normalenvektor[1]);
  MainForm.StaticTxtNormalenvektorZ.Caption := FloatToStr(ebene.normalenvektor[2]);

  entgrauen();
end;



// Als Sekundärebene speichern

procedure TMainForm.BtnSekEbeneSpeichernClick(Sender: TObject);
var testnv1,testnv2: vektor;
begin
  sekundaerebene.Create;

  case PgCtrlEbenenEingabe.TabIndex of
    0: begin // falls Parameterform
      testnv1[0] := FltSpnEdtRVx1.Value;
      testnv1[1] := FltSpnEdtRVy1.Value;
      testnv1[2] := FltSpnEdtRVz1.Value;

      testnv2[0] := FltSpnEdtRVx2.Value;
      testnv2[1] := FltSpnEdtRVy2.Value;
      testnv2[2] := FltSpnEdtRVz2.Value;

      if (kreuzprodukt(testnv1,testnv2)[0] = 0) and (kreuzprodukt(testnv1,testnv2)[1] = 0) and (kreuzprodukt(testnv1,testnv2)[2] = 0) then begin
        Application.MessageBox(PChar(i18nZerovectorwarn),PChar(i18nError),MB_ICONERROR);
        exit;
      end else begin
        sekundaerebene.stuetzvektor[0] := FltSpnEdtSVx.Value;
        sekundaerebene.stuetzvektor[1] := FltSpnEdtSVy.Value;
        sekundaerebene.stuetzvektor[2] := FltSpnEdtSVz.Value;

        sekundaerebene.richtungsvektor1[0] := FltSpnEdtRVx1.Value;
        sekundaerebene.richtungsvektor1[1] := FltSpnEdtRVy1.Value;
        sekundaerebene.richtungsvektor1[2] := FltSpnEdtRVz1.Value;

        sekundaerebene.richtungsvektor2[0] := FltSpnEdtRVx2.Value;
        sekundaerebene.richtungsvektor2[1] := FltSpnEdtRVy2.Value;
        sekundaerebene.richtungsvektor2[2] := FltSpnEdtRVz2.Value;

        sekundaerebene.genNormalenvektor();
        sekundaerebene.genKoordinatenform();
      end;
    end;

    1: begin // falls Normalenform
      if (FltSpnEdtNormalenNVx.Value = 0) and (FltSpnEdtNormalenNVy.Value = 0) and (FltSpnEdtNormalenNVz.Value = 0) then begin
        Application.MessageBox(PChar(i18nZerovectorwarn),PChar(i18nError),MB_ICONERROR);
        exit;
      end else begin
        sekundaerebene.stuetzvektor[0] := FltSpnEdtNormalenSVx.Value;
        sekundaerebene.stuetzvektor[1] := FltSpnEdtNormalenSVy.Value;
        sekundaerebene.stuetzvektor[2] := FltSpnEdtNormalenSVz.Value;

        sekundaerebene.normalenvektor[0] := FltSpnEdtNormalenNVx.Value;
        sekundaerebene.normalenvektor[1] := FltSpnEdtNormalenNVy.Value;
        sekundaerebene.normalenvektor[2] := FltSpnEdtNormalenNVz.Value;

        sekundaerebene.genKoordinatenform();
        sekundaerebene.genParameterform();
      end;

    end;

    2: begin // falls Koordinatenform
      if (FltSpnEdtKoordX.Value = 0) and (FltSpnEdtKoordY.Value = 0) and (FltSpnEdtKoordZ.Value = 0) then begin
        Application.MessageBox(PChar(i18nZerovectorwarn),PChar(i18nError),MB_ICONERROR);
        exit;
      end else begin
        sekundaerebene.normalenvektor[0] := FltSpnEdtKoordX.Value;
        sekundaerebene.normalenvektor[1] := FltSpnEdtKoordY.Value;
        sekundaerebene.normalenvektor[2] := FltSpnEdtKoordZ.Value;

        sekundaerebene.koordinatenform := FloatToStr(FltSpnEdtKoordinatenG.Value) + '=' +
                                 FloatToStr(sekundaerebene.normalenvektor[0]) + 'x' +
                                 vorzeichen(sekundaerebene.normalenvektor[1]) + 'y' +
                                 vorzeichen(sekundaerebene.normalenvektor[2]) + 'z';

        sekundaerebene.genNormalenform();
        sekundaerebene.genParameterform();
      end;
    end;
  end;

  sekundaerebene.genNormaleneinheitsvektor();

  entgrauenF();

  Application.MessageBox(PChar(i18nPlaneSaved),PChar(i18nSuccess),MB_ICONINFORMATION);

  sekundaerebene.free;
end;



// Orthogonale Gerade generieren

procedure TMainForm.BitBtnOrthGeradeOKClick(Sender: TObject);
begin
  StaticTxOrthGerSPx.Caption := FloatToStr(FltSpnEdtSPunktX.Value);
  StaticTxOrthGerSPy.Caption := FloatToStr(FltSpnEdtSPunktY.Value);
  StaticTxOrthGerSPz.Caption := FloatToStr(FltSpnEdtSPunktZ.Value);

  StaticTxOrthGerRVx.Caption := FloatToStr(ebene.normalenvektor[0]);
  StaticTxOrthGerRVy.Caption := FloatToStr(ebene.normalenvektor[1]);
  StaticTxOrthGerRVz.Caption := FloatToStr(ebene.normalenvektor[2]);
end;

procedure TMainForm.BtnDateiOeffnenClick(Sender: TObject);
begin

end;



// Stuetzvektor als Schnittpunkt für orthogonale Gerade nehmen

procedure TMainForm.BtnOrthSVClick(Sender: TObject);
begin
  FltSpnEdtSPunktX.Value := ebene.stuetzvektor[0];
  FltSpnEdtSPunktY.Value := ebene.stuetzvektor[1];
  FltSpnEdtSPunktZ.Value := ebene.stuetzvektor[2];

  StaticTxOrthGerSPx.Caption := FloatToStr(FltSpnEdtSPunktX.Value);
  StaticTxOrthGerSPy.Caption := FloatToStr(FltSpnEdtSPunktY.Value);
  StaticTxOrthGerSPz.Caption := FloatToStr(FltSpnEdtSPunktZ.Value);

  StaticTxOrthGerRVx.Caption := FloatToStr(ebene.normalenvektor[0]);
  StaticTxOrthGerRVy.Caption := FloatToStr(ebene.normalenvektor[1]);
  StaticTxOrthGerRVz.Caption := FloatToStr(ebene.normalenvektor[2]);
end;



// Punktprobe-Button

procedure TMainForm.BitBtnCheckPunktClick(Sender: TObject);
begin
  checkpunkt[0] := FltSpnEdtPunktX.Value;
  checkpunkt[1] := FltSpnEdtPunktY.Value;
  checkpunkt[2] := FltSpnEdtPunktZ.Value;

  if ebene.punktprobe(checkpunkt) = true then begin
    StaticTxtErgebnisPunktcheck.Caption := 'P ∈ E';
    ImgPunktprobeErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\checkmark.png');

    StaticTxtSpiegelPunkt.Caption := 'P′ = P';
    StaticTxtLotfusspunkt.Caption := 'L = P';
  end else begin
    StaticTxtErgebnisPunktcheck.Caption := 'P ∉ E';
    ImgPunktprobeErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\crossmark.png');

    StaticTxtSpiegelPunkt.Caption := 'P′(' + FloatToStr(ebene.spiegelpunkt(checkpunkt)[0]) +
                                     '; '   + FloatToStr(ebene.spiegelpunkt(checkpunkt)[1]) +
                                     '; '   + FloatToStr(ebene.spiegelpunkt(checkpunkt)[2]) + ')';

    StaticTxtLotfusspunkt.Caption := 'L(' + FloatToStr(ebene.lotfusspunkt(checkpunkt)[0]) +
                                     '; '  + FloatToStr(ebene.lotfusspunkt(checkpunkt)[1]) +
                                     '; '  + FloatToStr(ebene.lotfusspunkt(checkpunkt)[2]) + ')';
  end;
end;



// Schnittwinkel berechnen

procedure TMainForm.BitBtnWinkelOKClick(Sender: TObject);
var geradeRV: vektor;
begin
  if RadioBtnWinkelF.Checked = true then begin
    StaticTxtWinkel.Caption := schnittwinkel('e-e',ebene.normalenvektor,sekundaerebene.normalenvektor);
  end else begin
    geradeRV[0] := FltSpnEdtGeradeRVx.Value;
    geradeRV[1] := FltSpnEdtGeradeRVy.Value;
    geradeRV[2] := FltSpnEdtGeradeRVz.Value;

    StaticTxtWinkel.Caption := schnittwinkel('e-g',ebene.normalenvektor,geradeRV);
  end;
end;



// Abstand zwischen Ebene/Ebene, Ebene/Gerade oder Ebene/Punkt

procedure TMainForm.BitBtnAbstandOKClick(Sender: TObject);
var geradeSV,geradeRV,qv: vektor; q: punkt;
begin
  if RadioBtnAbstandEF.Checked = true then begin
    StaticTxtAbstand.Caption := abstand(ebene,sekundaerebene.stuetzvektor,sekundaerebene.normalenvektor,'F');

  end else if RadioBtnAbstandEg.Checked = true then begin
    geradeSV[0] := FltSpnEdtGeradeSVx1.Value;
    geradeSV[1] := FltSpnEdtGeradeSVy1.Value;
    geradeSV[2] := FltSpnEdtGeradeSVz1.Value;

    geradeRV[0] := FltSpnEdtGeradeRVx1.Value;
    geradeRV[1] := FltSpnEdtGeradeRVy1.Value;
    geradeRV[2] := FltSpnEdtGeradeRVz1.Value;

    StaticTxtAbstand.Caption := abstand(ebene,geradeSV,geradeRV,'g');

  end else begin
    q[0] := FltSpnEdtPunktX1.Value;
    q[1] := FltSpnEdtPunktY1.Value;
    q[2] := FltSpnEdtPunktZ1.Value;
    qv := pseudovektor(q);

    StaticTxtAbstand.Caption := abstand(ebene,qv,qv,'P');

  end;
end;



// Orthogonalität/Parallelität zweier Vektoren prüfen

procedure TMainForm.BitBtnVektorCheckOKClick(Sender: TObject);
var checkvektor1,checkvektor2: vektor;
begin
  checkvektor1[0] := FltSpnEdtCheckVektorX1.Value;
  checkvektor1[1] := FltSpnEdtCheckVektorY1.Value;
  checkvektor1[2] := FltSpnEdtCheckVektorZ1.Value;

  checkvektor2[0] := FltSpnEdtCheckVektorX2.Value;
  checkvektor2[1] := FltSpnEdtCheckVektorY2.Value;
  checkvektor2[2] := FltSpnEdtCheckVektorZ2.Value;

  if ((checkvektor1[0] = 0) and (checkvektor1[1] = 0) and (checkvektor1[2] = 0) or
      (checkvektor2[0] = 0) and (checkvektor2[1] = 0) and (checkvektor2[2] = 0)) then begin
    Application.MessageBox(PChar(i18nInvalidVectorWarn),PChar(i18nInvalidVector),MB_ICONERROR);
    exit;
  end;

  if ComboBoxCheckVektor.ItemIndex = 0 then begin
    if orthogonalcheck(checkvektor1,checkvektor2) = true then begin
      StaticTxtErgebnisVektorCheck1.Caption := i18nPerpendicular;
      ImgVektorcheckErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\checkmark.png');
    end else begin
      StaticTxtErgebnisVektorCheck1.Caption := i18nNotPerpendicular;
      ImgVektorcheckErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\crossmark.png');
    end;
  end else begin
    if parallelcheck(checkvektor1,checkvektor2) = true then begin
      StaticTxtErgebnisVektorCheck1.Caption := i18nParallel;
      ImgVektorcheckErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\checkmark.png');
    end else begin
      StaticTxtErgebnisVektorCheck1.Caption := i18nNotParallel;
      ImgVektorcheckErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\crossmark.png');
    end;
  end;
end;



// Werkzeuge: Kreuzprodukt berechnen

procedure TMainForm.BitBtnKreuzproduktOKClick(Sender: TObject);
var vektor1,vektor2: vektor;
begin
  vektor1[0] :=  FltSpnEdtKreuzproduktVektor1X.Value;
  vektor1[1] :=  FltSpnEdtKreuzproduktVektor1Y.Value;
  vektor1[2] :=  FltSpnEdtKreuzproduktVektor1Z.Value;

  vektor2[0] :=  FltSpnEdtKreuzproduktVektor2X.Value;
  vektor2[1] :=  FltSpnEdtKreuzproduktVektor2Y.Value;
  vektor2[2] :=  FltSpnEdtKreuzproduktVektor2Z.Value;

  StaticTxtKreuzproduktX.Caption := FloatToStr(kreuzprodukt(vektor1,vektor2)[0]);
  StaticTxtKreuzproduktY.Caption := FloatToStr(kreuzprodukt(vektor1,vektor2)[1]);
  StaticTxtKreuzproduktZ.Caption := FloatToStr(kreuzprodukt(vektor1,vektor2)[2]);
end;



// sonstige JSON-Daten laden

procedure ebeneSonstDatenLaden(jsondaten:string);
var echteJSONdaten: TJSONData;
begin
  echteJSONdaten := GetJSON(jsondaten);

  MainForm.FltSpnEdtPunktX.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('P[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtPunktY.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('P[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtPunktZ.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('P[2]').AsJSON,'"',''));

  MainForm.StaticTxtSpiegelPunkt.Caption := ReplaceText(echteJSONdaten.FindPath('P' + Chr(39)).AsJSON,'"','');

  MainForm.StaticTxtLotfusspunkt.Caption := ReplaceText(echteJSONdaten.FindPath('L').AsJSON,'"','');

  MainForm.StaticTxtErgebnisPunktcheck.Caption := ReplaceText(echteJSONdaten.FindPath('PPergebnis').AsJSON,'"','');

  if MainForm.StaticTxtErgebnisPunktcheck.Caption = 'P ∈ E' then begin
    MainForm.ImgPunktprobeErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\checkmark.png');
  end else if MainForm.StaticTxtErgebnisPunktcheck.Caption = 'P ∉ E' then begin
    MainForm.ImgPunktprobeErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\crossmark.png');
  end else begin
    MainForm.ImgPunktprobeErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\questionmark.png');
  end;

  MainForm.FltSpnEdtPunktX1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('Q[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtPunktY1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('Q[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtPunktZ1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('Q[2]').AsJSON,'"',''));

  MainForm.FltSpnEdtGeradeSVx1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('g.stuetzvektor[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeSVy1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('g.stuetzvektor[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeSVz1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('g.stuetzvektor[2]').AsJSON,'"',''));

  MainForm.FltSpnEdtGeradeRVx1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('g.richtungsvektor[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeRVy1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('g.richtungsvektor[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeRVz1.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('g.richtungsvektor[2]').AsJSON,'"',''));

  MainForm.StaticTxtAbstand.Caption := ReplaceText(echteJSONdaten.FindPath('d').AsJSON,'"','');

  case ReplaceText(echteJSONdaten.FindPath('abstandsart').AsJSON,'"','') of
    'F': begin
      MainForm.RadioBtnAbstandEF.Checked := True;
    end;
    'g': begin
      MainForm.RadioBtnAbstandEg.Checked := True;
    end;
    'Q': begin
      MainForm.RadioBtnAbstandEP.Checked := True;
    end;
  end;

  MainForm.FltSpnEdtSPunktX.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('S[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtSPunktY.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('S[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtSPunktZ.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('S[2]').AsJSON,'"',''));

  MainForm.StaticTxOrthGerSPx.Caption := ReplaceText(echteJSONdaten.FindPath('h.stuetzvektor[0]').AsJSON,'"','');
  MainForm.StaticTxOrthGerSPy.Caption := ReplaceText(echteJSONdaten.FindPath('h.stuetzvektor[1]').AsJSON,'"','');
  MainForm.StaticTxOrthGerSPz.Caption := ReplaceText(echteJSONdaten.FindPath('h.stuetzvektor[2]').AsJSON,'"','');

  MainForm.StaticTxOrthGerRVx.Caption := ReplaceText(echteJSONdaten.FindPath('h.richtungsvektor[0]').AsJSON,'"','');
  MainForm.StaticTxOrthGerRVy.Caption := ReplaceText(echteJSONdaten.FindPath('h.richtungsvektor[1]').AsJSON,'"','');
  MainForm.StaticTxOrthGerRVz.Caption := ReplaceText(echteJSONdaten.FindPath('h.richtungsvektor[2]').AsJSON,'"','');

  MainForm.FltSpnEdtGeradeSVx.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('i.stuetzvektor[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeSVy.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('i.stuetzvektor[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeSVz.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('i.stuetzvektor[2]').AsJSON,'"',''));

  MainForm.FltSpnEdtGeradeRVx.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('i.richtungsvektor[0]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeRVy.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('i.richtungsvektor[1]').AsJSON,'"',''));
  MainForm.FltSpnEdtGeradeRVz.Value := StrToFloat(ReplaceText(echteJSONdaten.FindPath('i.richtungsvektor[2]').AsJSON,'"',''));

  MainForm.StaticTxtWinkel.Caption := ReplaceText(echteJSONdaten.FindPath('winkel').AsJSON,'"','');
end;



// Datei öffnen

procedure TMainForm.MenuDateiOeffnenClick(Sender: TObject);
var ebenendatei: TStringList; ebenendateiJSON: TJSONData; ebenendateiEinzeiler: string; i: integer;
begin
  OpenDlgMain.DefaultExt := '.planez';
  OpenDlgMain.Filter := 'Vector Planez File|*.planez';
  OpenDlgMain.Title := i18nLoadFile;

  if OpenDlgMain.Execute then begin
    if fileExists(OpenDlgMain.Filename) then begin
      ebenendatei := TStringList.Create;
      try
        ebenendatei.LoadFromFile(OpenDlgMain.Filename);

        if StaticTxtGrauWarn.Caption = '' then begin

          with TTaskDialog.Create(self) do
            try
              Title := i18nReplacePlane;
              Caption := i18nConfirm;
              Text := i18nReplacePlaneDesc;
              CommonButtons := [];
              with TTaskDialogButtonItem(Buttons.Add) do
              begin
                Caption := i18nReplace;
                ModalResult := mrYes;
              end;
              with TTaskDialogButtonItem(Buttons.Add) do
              begin
                Caption := i18nKeep;
                ModalResult := mrNo;
              end;
              MainIcon := tdiWarning;
              if Execute then
                if ModalResult = mrNo then begin
                  exit;
                end;
            finally
              Free;
            end;

        end else begin
          ebene.Create;
        end;

        ebenendateiEinzeiler := '';

        for i:=0 to ebenendatei.Count - 1 do begin
          ebenendateiEinzeiler := ebenendateiEinzeiler + ebenendatei[i];
        end;

        try
          ebenendateiJSON := GetJSON(ebenendateiEinzeiler);
          ebene.ebeneLaden(ebenendateiJSON.FindPath('ebene').AsJSON);
          ebeneSonstDatenLaden(ebenendateiEinzeiler);
          ebeneSpeichernRest();

          if ReplaceText(ebenendateiJSON.FindPath('sekundaerebene.aktiv').AsJSON,'"','') = 'true' then begin
            sekundaerebene.Create;
            sekundaerebene.ebeneLaden(ebenendateiJSON.FindPath('sekundaerebene').AsJSON);
            sekundaerebene.genNormaleneinheitsvektor();
            entgrauenF();
          end;

          PgCtrlAktionen.TabIndex := 1;
          Application.MessageBox(PChar(i18nPlaneLoaded),PChar(i18nSuccess),MB_ICONINFORMATION);

        except
          On E:Exception do begin // Wenn die Datei fehlerhaft ist
            fehlermeldungDateifehler(E.Message);
          end;
        end;

      finally
        ebenendatei.Free;
        ebene.Free;
      end;
    end else begin
      Application.MessageBox(PChar(i18nFileDoesntExist),PChar(i18nError), MB_ICONERROR)
    end;
  end else begin
    Application.MessageBox(PChar(i18nNoFileSelected),PChar(i18nError), MB_ICONERROR);
  end;

end;



// sonstige JSON-Daten generieren

function genSonstJSONdaten():string;
var ergebnis: string;
begin
  result := '';
end;



// Ebene in Datei speichern

procedure TMainForm.MenuDateiSpeichernClick(Sender: TObject);
var dateiInhalt: TStringlist; jsondaten: string; jsondatenobj,ebenejsondatensubobj,sekundaerebenejsondatensubobj,gjsonsubobj,hjsonsubobj,ijsonsubobj: TJSONObject;
begin
  if StaticTxtGrauWarn.Caption = '' then begin

    SaveDlgMain.FileName := 'ebene ' + FormatDateTime('dd.mm.yyyy hh-mm-ss',Now) + '.planez'; // vorgeschlagener Dateiname: "ebene 09.06.2013 19-24-55"
    SaveDlgMain.DefaultExt := '.planez';
    SaveDlgMain.Filter := 'Vector Planez File|*.planez';
    SaveDlgMain.Title := i18nSaveFileAs;

    if SaveDlgMain.Execute then begin
      if fileExists(SaveDlgMain.Filename) then begin
        with TTaskDialog.Create(self) do
          try
            Title := i18nReplaceFile;
            Caption := i18nConfirm;
            Text := i18nReplaceFileDesc;
            CommonButtons := [];

            with TTaskDialogButtonItem(Buttons.Add) do
            begin
              Caption := i18nReplace;
              ModalResult := mrYes;
            end;

            with TTaskDialogButtonItem(Buttons.Add) do
            begin
              Caption := i18nKeep;
              ModalResult := mrNo;
            end;

            MainIcon := tdiWarning;

            if Execute then begin
              if ModalResult = mrNo then begin
                exit;
              end;
            end;

          finally
            Free;
          end;
      end;

      dateiInhalt := TStringlist.Create;
      jsondatenobj := TJSONObject.Create;
      ebenejsondatensubobj := TJSONObject.Create;
      sekundaerebenejsondatensubobj := TJSONObject.Create;
      gjsonsubobj := TJSONObject.Create;
      hjsonsubobj := TJSONObject.Create;
      ijsonsubobj := TJSONObject.Create;

      ebenejsondatensubobj.Add('stuetzvektor',TJSONArray.Create([
                                                                 FloatToStr(ebene.stuetzvektor[0]),
                                                                 FloatToStr(ebene.stuetzvektor[1]),
                                                                 FloatToStr(ebene.stuetzvektor[2])
                                                                 ]));

      ebenejsondatensubobj.Add('richtungsvektor1',TJSONArray.Create([
                                                                     FloatToStr(ebene.richtungsvektor1[0]),
                                                                     FloatToStr(ebene.richtungsvektor1[1]),
                                                                     FloatToStr(ebene.richtungsvektor1[2])
                                                                     ]));

      ebenejsondatensubobj.Add('richtungsvektor2',TJSONArray.Create([
                                                                     FloatToStr(ebene.richtungsvektor2[0]),
                                                                     FloatToStr(ebene.richtungsvektor2[1]),
                                                                     FloatToStr(ebene.richtungsvektor2[2])
                                                                     ]));

      ebenejsondatensubobj.Add('normalenvektor',TJSONArray.Create([
                                                                   FloatToStr(ebene.normalenvektor[0]),
                                                                   FloatToStr(ebene.normalenvektor[1]),
                                                                   FloatToStr(ebene.normalenvektor[2])
                                                                   ]));

      ebenejsondatensubobj.Add('normaleneinheitsvektor',TJSONArray.Create([
                                                                           FloatToStr(ebene.normaleneinheitsvektor[0]),
                                                                           FloatToStr(ebene.normaleneinheitsvektor[1]),
                                                                           FloatToStr(ebene.normaleneinheitsvektor[2])
                                                                           ]));

      ebenejsondatensubobj.Add('normaleneinheitsvektorILSW',TJSONArray.Create([
                                                                               ebene.normaleneinheitsvektorILSW[0],
                                                                               ebene.normaleneinheitsvektorILSW[1],
                                                                               ebene.normaleneinheitsvektorILSW[2]
                                                                               ]));

      ebenejsondatensubobj.Add('koordinatengleich',FloatToStr(ebene.koordinatengleich));
      ebenejsondatensubobj.Add('koordinatenform',ebene.koordinatenform);

      jsondatenobj.Add('ebene',ebenejsondatensubobj);

      if RadioBtnAbstandEF.Enabled = True then begin // herausfinden, ob eine Sekundärebene gespeichert wurde
        sekundaerebenejsondatensubobj.Add('aktiv','true');
        sekundaerebenejsondatensubobj.Add('stuetzvektor',TJSONArray.Create([
                                                                 FloatToStr(sekundaerebene.stuetzvektor[0]),
                                                                 FloatToStr(sekundaerebene.stuetzvektor[1]),
                                                                 FloatToStr(sekundaerebene.stuetzvektor[2])
                                                                 ]));

        sekundaerebenejsondatensubobj.Add('richtungsvektor1',TJSONArray.Create([
                                                                       FloatToStr(sekundaerebene.richtungsvektor1[0]),
                                                                       FloatToStr(sekundaerebene.richtungsvektor1[1]),
                                                                       FloatToStr(sekundaerebene.richtungsvektor1[2])
                                                                       ]));

        sekundaerebenejsondatensubobj.Add('richtungsvektor2',TJSONArray.Create([
                                                                       FloatToStr(sekundaerebene.richtungsvektor2[0]),
                                                                       FloatToStr(sekundaerebene.richtungsvektor2[1]),
                                                                       FloatToStr(sekundaerebene.richtungsvektor2[2])
                                                                       ]));

        sekundaerebenejsondatensubobj.Add('normalenvektor',TJSONArray.Create([
                                                                     FloatToStr(sekundaerebene.normalenvektor[0]),
                                                                     FloatToStr(sekundaerebene.normalenvektor[1]),
                                                                     FloatToStr(sekundaerebene.normalenvektor[2])
                                                                     ]));

        sekundaerebenejsondatensubobj.Add('normaleneinheitsvektor',TJSONArray.Create([
                                                                             FloatToStr(sekundaerebene.normaleneinheitsvektor[0]),
                                                                             FloatToStr(sekundaerebene.normaleneinheitsvektor[1]),
                                                                             FloatToStr(sekundaerebene.normaleneinheitsvektor[2])
                                                                             ]));

        sekundaerebenejsondatensubobj.Add('normaleneinheitsvektorILSW',TJSONArray.Create([
                                                                                 sekundaerebene.normaleneinheitsvektorILSW[0],
                                                                                 sekundaerebene.normaleneinheitsvektorILSW[1],
                                                                                 sekundaerebene.normaleneinheitsvektorILSW[2]
                                                                                 ]));

        sekundaerebenejsondatensubobj.Add('koordinatengleich',FloatToStr(sekundaerebene.koordinatengleich));
        sekundaerebenejsondatensubobj.Add('koordinatenform',sekundaerebene.koordinatenform);
      end else begin
        sekundaerebenejsondatensubobj.Add('aktiv','false');
      end;

      jsondatenobj.Add('sekundaerebene',sekundaerebenejsondatensubobj);

      jsondatenobj.Add('P',TJSONArray.Create([
                                              FloatToStr(MainForm.FltSpnEdtPunktX.Value),
                                              FloatToStr(MainForm.FltSpnEdtPunktY.Value),
                                              FloatToStr(MainForm.FltSpnEdtPunktZ.Value)
                                              ]));

      jsondatenobj.Add('P' + Chr(39),MainForm.StaticTxtSpiegelPunkt.Caption);
      jsondatenobj.Add('L',MainForm.StaticTxtLotfusspunkt.Caption);
      jsondatenobj.Add('PPergebnis',MainForm.StaticTxtErgebnisPunktcheck.Caption);

      jsondatenobj.Add('Q',TJSONArray.Create([
                                              FloatToStr(MainForm.FltSpnEdtPunktX1.Value),
                                              FloatToStr(MainForm.FltSpnEdtPunktY1.Value),
                                              FloatToStr(MainForm.FltSpnEdtPunktZ1.Value)
                                              ]));

      gjsonsubobj.Add('stuetzvektor',TJSONArray.Create([
                                                        FloatToStr(MainForm.FltSpnEdtGeradeSVx1.Value),
                                                        FloatToStr(MainForm.FltSpnEdtGeradeSVy1.Value),
                                                        FloatToStr(MainForm.FltSpnEdtGeradeSVz1.Value)
                                                        ]));

      gjsonsubobj.Add('richtungsvektor',TJSONArray.Create([
                                                        FloatToStr(MainForm.FltSpnEdtGeradeRVx1.Value),
                                                        FloatToStr(MainForm.FltSpnEdtGeradeRVy1.Value),
                                                        FloatToStr(MainForm.FltSpnEdtGeradeRVz1.Value)
                                                        ]));

      jsondatenobj.Add('g',gjsonsubobj);

      jsondatenobj.Add('d',MainForm.StaticTxtAbstand.Caption);

      if MainForm.RadioBtnAbstandEF.Checked = True then begin
        jsondatenobj.Add('abstandsart','F');
      end else if MainForm.RadioBtnAbstandEg.Checked = True then begin
        jsondatenobj.Add('abstandsart','g');
      end else begin
        jsondatenobj.Add('abstandsart','Q');
      end;

      jsondatenobj.Add('S',TJSONArray.Create([
                                              FloatToStr(MainForm.FltSpnEdtSPunktX.Value),
                                              FloatToStr(MainForm.FltSpnEdtSPunktY.Value),
                                              FloatToStr(MainForm.FltSpnEdtSPunktZ.Value)
                                              ]));

      hjsonsubobj.Add('stuetzvektor',TJSONArray.Create([
                                                        MainForm.StaticTxOrthGerSPx.Caption,
                                                        MainForm.StaticTxOrthGerSPy.Caption,
                                                        MainForm.StaticTxOrthGerSPz.Caption
                                                        ]));

      hjsonsubobj.Add('richtungsvektor',TJSONArray.Create([
                                                           MainForm.StaticTxOrthGerRVx.Caption,
                                                           MainForm.StaticTxOrthGerRVx.Caption,
                                                           MainForm.StaticTxOrthGerRVx.Caption
                                                           ]));

      jsondatenobj.Add('h',hjsonsubobj);

      ijsonsubobj.Add('stuetzvektor',TJSONArray.Create([
                                                        FloatToStr(MainForm.FltSpnEdtGeradeSVx.Value),
                                                        FloatToStr(MainForm.FltSpnEdtGeradeSVy.Value),
                                                        FloatToStr(MainForm.FltSpnEdtGeradeSVz.Value)
                                                        ]));

      ijsonsubobj.Add('richtungsvektor',TJSONArray.Create([
                                                           FloatToStr(MainForm.FltSpnEdtGeradeRVx.Value),
                                                           FloatToStr(MainForm.FltSpnEdtGeradeRVy.Value),
                                                           FloatToStr(MainForm.FltSpnEdtGeradeRVz.Value)
                                                           ]));

      jsondatenobj.Add('i',ijsonsubobj);
      jsondatenobj.Add('winkel',MainForm.StaticTxtWinkel.Caption);


      dateiInhalt.Add(jsondatenobj.FormatJSON([foSingleLineArray,foUseTabchar]));

      // Diese Sub-Objekte müssen nicht extra "befreit" werden, da die übergeordneten Objekte als Ganzes "befreit" werden.
      // https://stackoverflow.com/questions/66970555/free-pascal-sigsegv-error-when-freeing-objects/66986314#66986314

      {ebenejsondatensubobj.Free;
      sekundaerebenejsondatensubobj.Free;
      ijsonsubobj.Free;
      hjsonsubobj.Free;
      gjsonsubobj.Free;}

      dateiInhalt.SaveToFile(SaveDlgMain.FileName); // Datei speichern
      Application.MessageBox(PChar(i18nFileSaved),PChar(i18nSaved), MB_ICONINFORMATION);

      jsondatenobj.Free;
      dateiInhalt.Free;
    end else begin
      Application.MessageBox(PChar(i18nAbortedSaving),PChar(i18nError), MB_ICONERROR);
    end;
  end else begin
    Application.MessageBox(PChar(i18nEnterPlaneFirst),PChar(i18nError), MB_ICONERROR);
  end;
end;



// Alles zurücksetzen

procedure TMainForm.MenuResetAllClick(Sender: TObject);
begin
  with TTaskDialog.Create(self) do
    try
      Title := i18nResetAll;
      Caption := i18nConfirm;
      Text := i18nResetQuestion;
      CommonButtons := [];

      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := i18nReset;
        ModalResult := mrYes;
      end;

      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := i18nCancel;
        ModalResult := mrNo;
      end;

      MainIcon := tdiWarning;

      if Execute then begin
        if ModalResult = mrYes then begin
          PgCtrlAktionen.TabIndex := 0;
          WerteZuruecksetzen();
          eingrauen();
          ebene.Reset();
          sekundaerebene.Reset();
        end;
      end;

    finally
      Free;
    end;
end;



// alle Werte zurücksetzen

procedure WerteZuruecksetzen();
begin
  MainForm.FltSpnEdtSVx.Value := 0;
  MainForm.FltSpnEdtSVy.Value := 0;
  MainForm.FltSpnEdtSVz.Value := 0;

  MainForm.FltSpnEdtRVx1.Value := 0;
  MainForm.FltSpnEdtRVy1.Value := 0;
  MainForm.FltSpnEdtRVz1.Value := 0;

  MainForm.FltSpnEdtRVx2.Value := 0;
  MainForm.FltSpnEdtRVy2.Value := 0;
  MainForm.FltSpnEdtRVz2.Value := 0;

  MainForm.FltSpnEdtNormalenSVx.Value := 0;
  MainForm.FltSpnEdtNormalenSVy.Value := 0;
  MainForm.FltSpnEdtNormalenSVz.Value := 0;

  MainForm.FltSpnEdtNormalenNVx.Value := 0;
  MainForm.FltSpnEdtNormalenNVy.Value := 0;
  MainForm.FltSpnEdtNormalenNVz.Value := 0;

  MainForm.FltSpnEdtKoordinatenG.Value := 0;
  MainForm.FltSpnEdtKoordX.Value := 0;
  MainForm.FltSpnEdtKoordY.Value := 0;
  MainForm.FltSpnEdtKoordZ.Value := 0;

  MainForm.StaticTxtParamformSVx.Caption := '0';
  MainForm.StaticTxtParamformSVy.Caption := '0';
  MainForm.StaticTxtParamformSVz.Caption := '0';

  MainForm.StaticTxtParamformRV1x.Caption := '0';
  MainForm.StaticTxtParamformRV1y.Caption := '0';
  MainForm.StaticTxtParamformRV1z.Caption := '0';

  MainForm.StaticTxtParamformRV2x.Caption := '0';
  MainForm.StaticTxtParamformRV2y.Caption := '0';
  MainForm.StaticTxtParamformRV2z.Caption := '0';

  MainForm.StaticTxtE2.Caption := '0';
  MainForm.StaticTxtNormalenformSVx.Caption := '0';
  MainForm.StaticTxtNormalenformSVy.Caption := '0';
  MainForm.StaticTxtNormalenformSVz.Caption := '0';

  MainForm.StaticTxtNormalenformNormalvektorX.Caption := '0';
  MainForm.StaticTxtNormalenformNormalvektorY.Caption := '0';
  MainForm.StaticTxtNormalenformNormalvektorZ.Caption := '0';

  MainForm.StaticTxtNormalenformNormalvektorX.Caption := '0';
  MainForm.StaticTxtNormalenformNormalvektorY.Caption := '0';
  MainForm.StaticTxtNormalenformNormalvektorZ.Caption := '0';

  MainForm.StaticTxtKoordinatenform.Caption := '0=0x+0y+0z';

  MainForm.StaticTxtNormalenvektorX.Caption := '0';
  MainForm.StaticTxtNormalenvektorY.Caption := '0';
  MainForm.StaticTxtNormalenvektorZ.Caption := '0';

  MainForm.StaticTxtSpiegelPunkt.Caption := 'P′(0; 0; 0)';
  MainForm.StaticTxtLotfusspunkt.Caption := 'L(0; 0; 0)';

  MainForm.FltSpnEdtPunktX.Value := 0;
  MainForm.FltSpnEdtPunktY.Value := 0;
  MainForm.FltSpnEdtPunktZ.Value := 0;

  MainForm.StaticTxtErgebnisPunktcheck.Caption := i18nUndefined;
  MainForm.ImgPunktprobeErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\questionmark.png');

  MainForm.RadioBtnAbstandEg.Checked := True;

  MainForm.FltSpnEdtGeradeSVx1.Value := 0;
  MainForm.FltSpnEdtGeradeSVy1.Value := 0;
  MainForm.FltSpnEdtGeradeSVz1.Value := 0;

  MainForm.FltSpnEdtGeradeRVx1.Value := 0;
  MainForm.FltSpnEdtGeradeRVy1.Value := 0;
  MainForm.FltSpnEdtGeradeRVz1.Value := 0;

  MainForm.FltSpnEdtPunktX1.Value := 0;
  MainForm.FltSpnEdtPunktY1.Value := 0;
  MainForm.FltSpnEdtPunktZ1.Value := 0;

  MainForm.StaticTxtAbstand.Caption := 'd = 0 LE';

  MainForm.FltSpnEdtSPunktX.Value := 0;
  MainForm.FltSpnEdtSPunktY.Value := 0;
  MainForm.FltSpnEdtSPunktZ.Value := 0;

  MainForm.StaticTxOrthGerSPx.Caption := '0';
  MainForm.StaticTxOrthGerSPy.Caption := '0';
  MainForm.StaticTxOrthGerSPz.Caption := '0';

  MainForm.StaticTxOrthGerRVx.Caption := '0';
  MainForm.StaticTxOrthGerRVy.Caption := '0';
  MainForm.StaticTxOrthGerRVz.Caption := '0';

  MainForm.RadioBtnWinkelg.Checked := True;

  MainForm.FltSpnEdtGeradeSVx.Value := 0;
  MainForm.FltSpnEdtGeradeSVy.Value := 0;
  MainForm.FltSpnEdtGeradeSVz.Value := 0;

  MainForm.FltSpnEdtGeradeRVx.Value := 0;
  MainForm.FltSpnEdtGeradeRVy.Value := 0;
  MainForm.FltSpnEdtGeradeRVz.Value := 0;

  MainForm.StaticTxtWinkel.Caption := 'ɣ = 0°';

  MainForm.FltSpnEdtKreuzproduktVektor1X.Value := 0;
  MainForm.FltSpnEdtKreuzproduktVektor1Y.Value := 0;
  MainForm.FltSpnEdtKreuzproduktVektor1Z.Value := 0;

  MainForm.FltSpnEdtKreuzproduktVektor2X.Value := 0;
  MainForm.FltSpnEdtKreuzproduktVektor2Y.Value := 0;
  MainForm.FltSpnEdtKreuzproduktVektor2Z.Value := 0;

  MainForm.StaticTxtKreuzproduktX.Caption := '0';
  MainForm.StaticTxtKreuzproduktY.Caption := '0';
  MainForm.StaticTxtKreuzproduktZ.Caption := '0';

  MainForm.FltSpnEdtCheckVektorX1.Value := 0;
  MainForm.FltSpnEdtCheckVektorY1.Value := 0;
  MainForm.FltSpnEdtCheckVektorZ1.Value := 0;

  MainForm.FltSpnEdtCheckVektorX2.Value := 0;
  MainForm.FltSpnEdtCheckVektorY2.Value := 0;
  MainForm.FltSpnEdtCheckVektorZ2.Value := 0;

  MainForm.ComboBoxCheckVektor.ItemIndex := 0;
  MainForm.ImgVektorcheckErgIcon.Picture.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\images\questionmark.png');
  MainForm.StaticTxtErgebnisVektorCheck1.Caption := i18nUndefined;
end;



// File Association
// https://wiki.freepascal.org/FileAssociation

procedure TMainForm.FormCreate(Sender: TObject);
var s,ebenendateiEinzeiler: String; ebenendateiJSON: TJSONData; ebenenjson: TStringList; i: integer;
begin
  TabCtrlNormalenVektor.Tabs[0] := i18nNormalvector;
  TabCtrlNormalenVektor.Tabs[1] := i18nNormalUnitVector;


  if ParamCount > 0 then begin
    s := ParamStr(1);

    if ExtractFileExt(s) = '.planez' then begin
      ebenenjson := TStringList.Create;
      ebene.Create;

      ebenenjson.LoadFromFile(s);

      ebenendateiEinzeiler := '';

      for i:=0 to ebenenjson.Count - 1 do begin
        ebenendateiEinzeiler := ebenendateiEinzeiler + ebenenjson[i];
      end;

      try
        ebenendateiJSON := GetJSON(ebenendateiEinzeiler);
        ebene.ebeneLaden(ebenendateiJSON.FindPath('ebene').AsJSON);
        ebeneSonstDatenLaden(ebenendateiEinzeiler);
        ebeneSpeichernRest();

        if ReplaceText(ebenendateiJSON.FindPath('sekundaerebene.aktiv').AsJSON,'"','') = 'true' then begin
          sekundaerebene.Create;
          sekundaerebene.ebeneLaden(ebenendateiJSON.FindPath('sekundaerebene').AsJSON);
          sekundaerebene.genNormaleneinheitsvektor();
          entgrauenF();
        end;

        PgCtrlAktionen.TabIndex := 1;
      except
        On E:Exception do begin
          fehlermeldungDateifehler(E.Message);
        end;
      end;

      ebenenjson.Free;
      ebene.Free;
    end else begin
      Application.MessageBox(PChar(i18nFileTypeWarn1  + ExtractFileExt(s) + i18nFileTypeWarn2),PChar(i18nInvalidFileType), MB_ICONERROR);
    end;
  end;

  assoc := TFileAssociation.Create(Self);

  assoc.ApplicationName := 'Vector Planez';
  assoc.ApplicationDescription := i18nProgrDescr;

  // you can change Extension and Action part for each extension you have

  assoc.Extension := '.planez';
  assoc.ExtensionName := 'Vector Planez File';
  assoc.ExtensionIcon := ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\icons\vectorplanez-24x24.ico';

  // full path required, you can use ParamStr(0) to get the path with the .exe name included. The path must be inside quotes if it has whitespace.
  assoc.Action := ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\vectorplanez.exe "%1"';
  assoc.ActionName := 'Open';
  assoc.ActionText := i18nLoadPlane;
  assoc.ActionIcon := ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\icons\vectorplanez-24x24.ico';

  // notice that using RegisterForAllUsers as True requires Administrator Privileges
  // if you want to run without privileges set it to false, but it will register for current user only
  assoc.RegisterForAllUsers:=False;

  if assoc.Execute then begin
    assoc.ClearIconCache; //<<-- rebuild icons
  end;
end;



// Fehler melden

procedure TMainForm.MenuBugReportClick(Sender: TObject);
begin
  OpenURL('https://codeberg.org/pixelcode/vector-planez/issues');
end;



// Dokumentation aufrufen

procedure TMainForm.MenuDokuClick(Sender: TObject);
begin
  // https://forum.lazarus.freepascal.org/index.php?topic=37669.0
  OpenDocument(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\documents\Dokumentation.pdf');
end;



// Sprache ändern

procedure spracheAendern(sprache:string);
begin

end;



// Sprache zu Deutsch ändern

procedure TMainForm.MenuDeutschClick(Sender: TObject);
begin
  with TTaskDialog.Create(self) do                    // Es ist mir nicht möglich, diesen TaskDialog in eine Prozedur zu verschieben, weil dann immer
    try                                               // die Fehlermeldung "Identifier not found: 'self'" kommt. Der TaskDialog funktioniert hier, also
      Title := i18nResetAllAndChangeLanguage;         // bleibt er hier. Dann steht der gleiche Code halt zweimal da (bei Englisch).
      Caption := i18nConfirm;
      Text := i18nResetLangQuestion;
      CommonButtons := [];
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := i18nResetAndChangeLanguage;
        ModalResult := mrYes;
      end;
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := i18nCancel;
        ModalResult := mrNo;
      end;
      MainIcon := tdiWarning;
      if Execute then
        if ModalResult = mrYes then begin
          PgCtrlAktionen.TabIndex := 0;
          WerteZuruecksetzen();
          eingrauen();
          ebene.Reset();
          sekundaerebene.Reset();

          SetDefaultLang('de');
          TabCtrlNormalenVektor.Tabs[0] := i18nNormalvector;
          TabCtrlNormalenVektor.Tabs[1] := i18nNormalUnitVector;
        end;
    finally
      Free;
    end;
end;



// Sprache zu Englisch ändern

procedure TMainForm.MenuEnglischClick(Sender: TObject);
begin
 with TTaskDialog.Create(self) do                      // Es ist mir nicht möglich, diesen TaskDialog in eine Prozedur zu verschieben, weil dann immer
    try                                                // die Fehlermeldung "Identifier not found: 'self'" kommt. Der TaskDialog funktioniert hier, also
      Title := i18nResetAllAndChangeLanguage;          // bleibt er hier. Dann steht der gleiche Code halt zweimal da (bei Deutsch).
      Caption := i18nConfirm;
      Text := i18nResetLangQuestion;
      CommonButtons := [];
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := i18nResetAndChangeLanguage;
        ModalResult := mrYes;
      end;
      with TTaskDialogButtonItem(Buttons.Add) do
      begin
        Caption := i18nCancel;
        ModalResult := mrNo;
      end;
      MainIcon := tdiWarning;
      if Execute then
        if ModalResult = mrYes then begin
          PgCtrlAktionen.TabIndex := 0;
          WerteZuruecksetzen();
          eingrauen();
          ebene.Reset();
          sekundaerebene.Reset();

          SetDefaultLang('en');
          TabCtrlNormalenVektor.Tabs[0] := i18nNormalvector;
          TabCtrlNormalenVektor.Tabs[1] := i18nNormalUnitVector;
        end;
    finally
      Free;
    end;
end;



// Über Vector Planez - Hilfe-Fenster anzeigen

procedure TMainForm.MenuUeberClick(Sender: TObject);
begin
  InfoForm.Show;
end;



// Zwischen Normalen- und Normaleneinheitsvektor wechseln

procedure TMainForm.TabCtrlNormalenVektorChange(Sender: TObject);
begin
  case TabCtrlNormalenVektor.TabIndex of
    0: begin
      StaticTxtNormalenvektorX.Caption := FloatToStr(ebene.normalenvektor[0]);
      StaticTxtNormalenvektorY.Caption := FloatToStr(ebene.normalenvektor[1]);
      StaticTxtNormalenvektorZ.Caption := FloatToStr(ebene.normalenvektor[2]);
    end;
    1: begin
      StaticTxtNormalenvektorX.Caption := ebene.normaleneinheitsvektorILSW[0];
      StaticTxtNormalenvektorY.Caption := ebene.normaleneinheitsvektorILSW[1];
      StaticTxtNormalenvektorZ.Caption := ebene.normaleneinheitsvektorILSW[2];
    end;
  end;
end;



// Dateifehler-Fehlermeldung

procedure fehlermeldungDateifehler(fehler:string);
begin
  Application.MessageBox(PChar(i18nInvalidFileWarnDesc + sLineBreak +  sLineBreak + i18nInvalidFileWarnDesc2 + sLineBreak + sLineBreak + fehler),PChar(i18nInvalidFile),MB_ICONERROR);
end;

end.

