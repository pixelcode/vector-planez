unit utranslationstrings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

// https://wiki.lazarus.freepascal.org/Step-by-step_instructions_for_creating_multi-language_applications

resourcestring
  i18nZerovectorwarn = 'Wie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach...';
  i18nError = 'Fehler';
  i18nWarning = 'Warnung';
  i18nPlaneSaved = 'Ebene erfolgreich gespeichert!';
  i18nFileSaved = 'Datei erfolgreich gespeichert';
  i18nSaved = 'Gespeichert';
  i18nPlaneLoaded = 'Ebene erfolgreich geladen!';
  i18nSuccess = 'Erfolg';
  i18nSavePlaneFirst = 'Speichere zunächst unter "Eingabe" eine Ebene!';
  i18nSavePlaneFirst2 = 'Speichere zunächst unter "Eingabe" eine Ebene E (und ggf. eine Sekundärebene F)!';
  i18nInvalidVector = 'Ungültiger Vektor';
  i18nInvalidVectorWarn = 'Mindestens ein Vektor ist ungültig, da er weder Betrag noch Richtung hat.';
  i18nPerpendicular = 'orthogonal';
  i18nNotPerpendicular = 'nicht orthogonal';
  i18nParallel = 'parallel';
  i18nNotParallel = 'nicht parallel';
  i18nFileTypeWarn1 = 'Der Dateityp ';
  i18nFileTypeWarn2 = ' wird nicht unterstützt. Bitte öffne nur .planez-Dateien!';
  i18nInvalidFileType = 'Ungültiges Dateiformat';
  i18nLoadPlane = 'Ebene laden';
  i18nLoadFile = 'Ebenen-Datei öffnen';
  i18nProgrDescr = 'Programm zum Rechnen mit Ebenen';
  i18nUndefined = 'undefiniert';
  i18nCancel = 'Abbrechen';
  i18nReset = 'Zurücksetzen';
  i18nResetAll = 'Alles zurücksetzen';
  i18nResetAndProceed = 'Zurücksetzen und weiter';
  i18nResetAndChangeLanguage = 'Zurücksetzen und Sprache ändern';
  i18nResetAllAndChangeLanguage = 'Alles zurücksetzen und Sprache ändern';
  i18nResetQuestion = 'Soll das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden?';
  i18nResetLangQuestion = 'Zum Ändern der Sprache muss das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden. Wollen Sie das jetzt tun?';
  i18nConfirm = 'Bestätigen';
  i18nInvalidFileWarnDesc = 'Ungültige JSON-Datei! Öffne ausschließlich unveränderte, direkt von Vector Planez generierte .planez-Dateien.';
  i18nInvalidFileWarnDesc2 = 'Beachte auch, dass Dateien aus dem Internet oder von Fremden ungültig sein oder Schadcode enthalten können.';
  i18nInvalidFile = 'Ungültige Datei';
  i18nReplace = 'Ersetzen';
  i18nReplacePlane = 'Ebene ersetzen';
  i18nReplaceFile = 'Datei ersetzen';
  i18nReplacePlaneDesc = 'Soll die bestehende Ebene ersetzt werden?';
  i18nReplaceFileDesc = 'Soll die bestehende Datei ersetzt werden?';
  i18nKeep = 'Behalten';
  i18nFileDoesntExist = 'Datei existiert nicht';
  i18nNoFileSelected = 'Du hast keine Datei ausgewählt!';
  i18nSaveFileAs = 'Ebenen-Datei speichern unter';
  i18nAbortedSaving = 'Du hast das Speichern abgebrochen';
  i18nEnterPlaneFirst = 'Gib zunächst eine Ebene ein!';
  i18nNormalvector = 'Normalenvektor';
  i18nNormalUnitVector = 'Normaleneinheitsvektor';
  i18nNotParallelWarn = 'Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Ebene F ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt.';
  i18nNotParallelWarn2 = 'Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Geraden g ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt.';
  i18nParallelWarn = 'Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Ebene F ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt.';
  i18nParallelWarn2 = 'Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Geraden i ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt.';

implementation

end.

