��    {      �  �   �      h
  ;   i
     �
  
   �
     �
     �
     �
     �
     �
     �
               !  .   6  !   e  	   �     �     �     �     �     �  '   �       m   4     �     �  p   �  g   <     �  	   �     �     �     �     �       !         5     V     q     �     �     �     �     �     �     �     �     	  
          
   $     /     8  S   D     �     �     �     �     �     �  J   �     7     N     ]     m     �     �  �   �     Z  
   b  
   m     x     �     �  R   �  )   �  )   '  T   Q  S   �  R   �  #   M     q  �   y       	   $     .  q   ?     �     �  I   �  �   &  �   �  �   x  �   &  �  �     ^     f  	   �  �   �       !        A     R  
   a     l  +   u  .   �  9   �  ;   
  7   F  7   ~  :   �  S   �  W   E  8   �  9   �  3        D  �   [  $   �  V     �   Y     �  �   �  1   �        
                     (      4      :      >      W      `      f   -   {   "   �      �      �      �      �   
   !  	   !     !     4!  [   O!      �!     �!  ]   �!  R   /"     �"  	   �"     �"     �"     �"     �"     �"     �"     �"     #     3#     M#  
   [#     f#     y#     �#     �#     �#  
   �#     �#     �#     �#     �#  
   �#     �#  J   �#     5$     J$     O$     ]$     p$     y$  P   �$     �$     �$     %  #   %     7%     E%  �   \%     �%  
   �%     �%     
&     &     *&  C   >&  %   �&  &   �&  E   �&  E   '  C   ['  $   �'     �'  �   �'     R(  	   g(     q(  W   ~(     �(     �(  M   �(  �   E)  �   �)  �   �*  �   ]+  k  ,     y-     �-     �-  e   �-     .     .     ,.     >.     K.     Y.     b.     i.  	   p.  	   z.     �.     �.     �.  !   �.  !   �.     �.     /     /     /  S   $/     x/  !   ~/  S   �/     �/         "   R       o   p       ^   @   B       f      K   ?       b      u   I   _       :           $   '   i   w       #                 J   &   Q       *          N       	       y      7           l   <   x   d   Z      A   3          O          r      5   W           /   z      t       S   (       v      +   ]   ,   1             8       U         g   m   !       
   n                  P   L   j   4      D   V              Y   `      [          >   X           {       s           =           %   E   )   G   .          ;   e   \   6      2          C   a   q       T   H          h   c      -   k       9             M   F   0     wird nicht unterstützt. Bitte öffne nur .planez-Dateien! &Bearbeiten &Berechnen &Datei &Dokumentation &Ebene speichern &Hilfe &OK &Schnittwinkel berechnen &Speichern unter &Öffnen &Über Vector Planez (dient als 2. Ebene bei diversen Berechnungen) * Bedingung: E ∥ F bzw. E ∥ g Abbrechen Abstand Abstand berechnen Abstand von Ebene E und Alles &zurücksetzen Alles zurücksetzen Alles zurücksetzen und Sprache ändern Als &Sekundärebene F speichern Beachte auch, dass Dateien aus dem Internet oder von Fremden ungültig sein oder Schadcode enthalten können. Bedingung: E ∦ F bzw. E ∦ g Behalten Bestimme hier den Abstand zwischen deiner Ebene E
und einer zweiten Ebene F, einer Gerade g oder einem Punkt Q.
 Bestimme hier den Schnittwinkel zwischen deiner Ebene E
und einer zweiten Ebene F oder einer Gerade i.
 Bestätigen Compiler: Datei erfolgreich gespeichert Datei ersetzen Datei existiert nicht Der Dateityp  Deutsch Du hast das Speichern abgebrochen Du hast keine Datei ausgewählt! Ebene erfolgreich geladen! Ebene erfolgreich gespeichert! Ebene ersetzen Ebene laden Ebenen-Datei speichern unter Ebenen-Datei öffnen Ebenen-Rechner Eingabe Englisch Entwickler: Erfolg Ergebnis:  Ersetzen Gerade g * Gerade i Gespeichert Gib hier deine Ebenengleichung in 
Parameter-, Normalen- oder Koordinatenform ein:
 Gib zunächst eine Ebene ein! Info Kreuzprodukt Lagebeziehung Lizenz: Lotfußpunkt: Mindestens ein Vektor ist ungültig, da er weder Betrag noch Richtung hat. Normaleneinheitsvektor Normalenvektor Orthogonalität Programm zum Rechnen mit Ebenen Programmfehler &melden Programmier-
sprache: 
 Prüfe hier, ob ein Punkt P auf der Ebene E liegt (diese in "Allgemein" speichern). 
Dazu einfach die Koordinaten unten eintragen und auf "Prüfen" klicken.
 Punkt Q Punktprobe Quellcode: Schnittwinkel Sekundärebene F Sekundärebene F * Soll das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden? Soll die bestehende Datei ersetzt werden? Soll die bestehende Ebene ersetzt werden? Speichere zunächst unter "Eingabe" eine 
Ebene E (und ggf. eine Sekundärebene F)!
 Speichere zunächst unter "Eingabe" eine Ebene E
(und ggf. eine Sekundärebene F)!
 Speichere zunächst unter "Eingabe" eine Ebene E (und ggf. eine Sekundärebene F)! Spiegelung des Punkts an der Ebene: Sprache Stelle hier die Gleichung einer Gerade h auf, welche die in "Allgemein" 
festgelegte Ebene E senkrecht schneidet. Gib zunächst den Schnittpunkt ein.
 Stützvektor nehmen Umformung Ungültige Datei Ungültige JSON-Datei! Öffne ausschließlich unveränderte, direkt von Vector Planez generierte .planez-Dateien. Ungültiger Vektor Ungültiges Dateiformat Vector Planez hat deine Ebenengleichung 
in folgende Formen umgewandelt:
 Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Ebene F ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Geraden g ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Ebene F ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Geraden i ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt. Vector Planez ist ein quelloffenes Programm zum 
Rechnen mit Ebenen aus der Mathematik. Diese
finden sich im Lehrplan der 12. gymnasialen Klassen-
stufe, weshalb ein entsprechender Wissensstand zur
Benutzung des Programms angeraten ist.

Berechnungen, die das Lösen von Gleichungen oder
Gleichungssystemen erfordern, unterstützt Vector
Planez leider nicht, da hierfür ein CAS nötig ist.
 Warnung Was soll Ebene E schneiden? Werkzeuge Zum Ändern der Sprache muss das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden. Wollen Sie das jetzt tun? Zurücksetzen Zurücksetzen und Sprache ändern nicht orthogonal nicht parallel orthogonal parallel tmainform.bitbtncheckpunkt.caption&Prüfen tmainform.bitbtnvektorcheckok.caption&Prüfen tmainform.statictxtergebnispunktcheck.captionundefiniert tmainform.statictxtergebnisvektorcheck1.captionundefiniert tmainform.statictxtgleichungsform.captionParameterform tmainform.statictxtgleichungsform1.captionNormalenform tmainform.statictxtgleichungsform2.captionKoordinatenform tmainform.statictxtgrauwarn.captionSpeichere zunächst unter "Eingabe" eine Ebene! tmainform.statictxtgrauwarnorth.captionSpeichere zunächst unter "Eingabe" eine Ebene! tmainform.tabsheeteingabeparamform.captionParameterform tmainform.tabsheetkoordinatenform.captionKoordinatenform tmainform.tabsheetnormalenform.captionNormalenform umain.i18nerrorFehler umain.i18nzerovectorwarnWie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach... utranslationstrings.i18nerrorFehler utranslationstrings.i18nsaveplanefirstSpeichere zunächst unter "Eingabe" eine Ebene! utranslationstrings.i18nzerovectorwarnWie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach... Über Vector Planez Content-Type: text/plain; charset=UTF-8
Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
  isn't supported. Please open .planez files only! &Edit &Calculate &File &Documentation &Save plane &Help &OK &Calculate cutting angle &Save as &Open &About Vector Planez (serves as 2nd plane in several calculations) * Condition: E ∥ F resp. E ∥ g Cancel Distance Calculate distance Distance of E and &Reset all Reset all Reset all and change language Save as &secondary plane F Also note that files from strangers or the internet may be invalid or even contain malware. Condition: E ∦ F resp. E ∦ g Keep Determine the distance between your plane E and a second
plane F, a straight g or a Point Q.
 Determine the cutting angle of your plane E and a second
plane F or a straight i.
 Confirm Compiler: File saved successfully Replace file File doesn't exist The file type  German You aborted saving the file You didn't select a file! Plane loaded successfully! Plane saved successfully! Replace plane Load plane Save plane file as Open planes file Plane calculator Input English Developer: Success Result: Replace Straight g * Straight i Saved Enter your plane equation here in 
parameter, normal or coordinates form:
 Enter a plane first! Info Cross product Situation relation Licence: Perpendicular base point: At least one vector is invalid because it doesn't have an amout nor a direction. Normal unit vector Normal vector Orthogonality Program for calculating with planes &Report a bug Programming
language:
 Check here whether a point P is part of the plane E (save it under "Input" first)
by entering the coordinates below and clicking on "Check".
 Point Q Point test Source code: Cutting angle Secondary plane F Secondary plane F * Should the whole program be reset including all inputs and outputs? Should the existing file be replaced? Should the existing plane be replaced? Save a plane E under "Input" first 
(and maybe a secondary plane F)!
 Save a plane E under "Input" first 
(and maybe a secondary plane F)!
 Save a plane E under "Input" first (and maybe a secondary plane F)! Mirroring of the point on the plane: Language Create the equation of a straight h which perpendicularly intersects the plane E
set under "Input". Enter the crossing point first.
 Take position vector Transform Invalid file Invalid JSON file! Only open unchanged .planez files directly created by Vector Planez. Invalid vector Invalid file type Vector Planez has transformed your plane 
equation into the following forms:
 Vector Planez has determined that your plane E is most likely not parallel to the plane F. Instead, it seems that E intersects F meaning the distance output is probably incorrect. Vector Planez has determined that your plane E is most likely not parallel to the straight g. Instead, it seems that E intersects g meaning the distance output is probably incorrect. Vector Planez has determined that your plane E is most likely parallel to the plane F which is why no cutting angle exists. In this case, the output is therefore incorrect. Vector Planez has determined that your plane E is most likely parallel to the straight i which is why no cutting angle exists. In this case, the output is therefore incorrect. Vector Planez is an open-source program for 
calculations with mathematic planes. Those 
are included in the curriculum of the 12th grade 
of the German Gymnasium (grammar school)
which is why appropriate knowledge is necessary.

Calculations that include solving equations or
equation systems aren't supported by Vector Planez 
because a CAS is needed for that.
 Warning What should intersect plane E? Tools To change the language, the whole program has to be reset, including all inputs and outputs. Proceed? Reset Reset and change language not perpendicular not parallel perpendicular parallel &Check &Check undefined undefined Parameter form Normal form Coordinate form Save a plane under "Input" first! Save a plane under "Input" first! Parameter form Coordinate form Normal form Error How do you imagine a plane perpendicular to the vector (0,0,0)? Think about that... Error Save a plane under "Input" first! How do you imagine a plane perpendicular to the vector (0,0,0)? Think about that... About Vector Planez 