��    }        �   �      �
  ;   �
     �
  
   �
     �
     �
     �
          
          '     8     A  .   V  !   �  	   �     �     �     �     �     �  '        4  m   T     �     �  p   �  g   \     �  	   �     �     �               +  !   3      U     v     �     �     �     �     �     �                    )  
   0     ;  
   D     O     X  S   d     �     �     �     �     �     �  J        W     n     }     �     �     �  �   �     z  
   �  
   �     �     �     �  R   �  )     )   G  T   q  S   �  R     #   m     �  �   �     0  	   D     N  q   _     �     �     �  I   
  �   T  �   �  �   �  �   T  �       �     �  	   �  �   �     ?  !   M     o     �  
   �     �  +   �  .   �  9   �  ;   8  7   t  7   �  :   �  S     W   s  8   �  9     3   >     r  �   �  $     V   0  -   �  �   �     E  �   Y  ;   ,      h   
   t            �      �      �      �      �      �      �      �   .   �   !   (!  	   J!     T!     \!     n!     �!     �!  '   �!     �!  m   �!     e"     �"  p   �"  g   �"     g#  	   s#     }#     �#     �#     �#     �#  !   �#      �#     $     4$     S$     b$     n$     �$     �$     �$     �$     �$     �$  
   �$     �$  
   �$     �$     �$  S   %     [%     y%     ~%     �%     �%     �%  J   �%     �%     &      &     0&     P&     g&  �   &     '  
   %'  
   0'     ;'     I'     Z'  R   m'  )   �'  )   �'  T   (  S   i(  R   �(  #   )     4)  �   <)     �)  	   �)     �)  q   *     t*     �*     �*  I   �*  �   �*  �   �+  �   I,  �   �,  �  �-     //     7/  	   S/  �   ]/     �/  !   �/     0     #0  
   20     =0     F0     O0     X0     d0     p0     ~0     �0  /   �0  /   �0     �0     	1     1     &1  h   -1     �1  /   �1     �1  h   �1     B2         "   R       p   q       _   @   B       g      K   ?   o   c      v   I   `       :           $   '   j   x       #                 J   &   Q       *          N       	       z      7           m   <   y   e   [      A   3          O          s      5   W           /   |      u       S   (       w      +   ^   ,   1             8       U         h   n   !       
   Z                  P   L   k   4      D   V              Y   a      \          >   X           }       t       {   =           %   E   )   G   .          ;   f   ]   6      2          C   b   r       T   H          i   d      -   l       9             M   F   0     wird nicht unterstützt. Bitte öffne nur .planez-Dateien! &Bearbeiten &Berechnen &Datei &Dokumentation &Ebene speichern &Hilfe &OK &Schnittwinkel berechnen &Speichern unter &Öffnen &Über Vector Planez (dient als 2. Ebene bei diversen Berechnungen) * Bedingung: E ∥ F bzw. E ∥ g Abbrechen Abstand Abstand berechnen Abstand von Ebene E und Alles &zurücksetzen Alles zurücksetzen Alles zurücksetzen und Sprache ändern Als &Sekundärebene F speichern Beachte auch, dass Dateien aus dem Internet oder von Fremden ungültig sein oder Schadcode enthalten können. Bedingung: E ∦ F bzw. E ∦ g Behalten Bestimme hier den Abstand zwischen deiner Ebene E
und einer zweiten Ebene F, einer Gerade g oder einem Punkt Q.
 Bestimme hier den Schnittwinkel zwischen deiner Ebene E
und einer zweiten Ebene F oder einer Gerade i.
 Bestätigen Compiler: Datei erfolgreich gespeichert Datei ersetzen Datei existiert nicht Der Dateityp  Deutsch Du hast das Speichern abgebrochen Du hast keine Datei ausgewählt! Ebene erfolgreich geladen! Ebene erfolgreich gespeichert! Ebene ersetzen Ebene laden Ebenen-Datei speichern unter Ebenen-Datei öffnen Ebenen-Rechner Eingabe Englisch Entwickler: Erfolg Ergebnis:  Ersetzen Gerade g * Gerade i Gespeichert Gib hier deine Ebenengleichung in 
Parameter-, Normalen- oder Koordinatenform ein:
 Gib zunächst eine Ebene ein! Info Kreuzprodukt Lagebeziehung Lizenz: Lotfußpunkt: Mindestens ein Vektor ist ungültig, da er weder Betrag noch Richtung hat. Normaleneinheitsvektor Normalenvektor Orthogonalität Programm zum Rechnen mit Ebenen Programmfehler &melden Programmier-
sprache: 
 Prüfe hier, ob ein Punkt P auf der Ebene E liegt (diese in "Allgemein" speichern). 
Dazu einfach die Koordinaten unten eintragen und auf "Prüfen" klicken.
 Punkt Q Punktprobe Quellcode: Schnittwinkel Sekundärebene F Sekundärebene F * Soll das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden? Soll die bestehende Datei ersetzt werden? Soll die bestehende Ebene ersetzt werden? Speichere zunächst unter "Eingabe" eine 
Ebene E (und ggf. eine Sekundärebene F)!
 Speichere zunächst unter "Eingabe" eine Ebene E
(und ggf. eine Sekundärebene F)!
 Speichere zunächst unter "Eingabe" eine Ebene E (und ggf. eine Sekundärebene F)! Spiegelung des Punkts an der Ebene: Sprache Stelle hier die Gleichung einer Gerade h auf, welche die in "Allgemein" 
festgelegte Ebene E senkrecht schneidet. Gib zunächst den Schnittpunkt ein.
 Stützvektor nehmen Umformung Ungültige Datei Ungültige JSON-Datei! Öffne ausschließlich unveränderte, direkt von Vector Planez generierte .planez-Dateien. Ungültiger Vektor Ungültiges Dateiformat Vector Planez Vector Planez hat deine Ebenengleichung 
in folgende Formen umgewandelt:
 Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Ebene F ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Geraden g ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Ebene F ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Geraden i ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt. Vector Planez ist ein quelloffenes Programm zum 
Rechnen mit Ebenen aus der Mathematik. Diese
finden sich im Lehrplan der 12. gymnasialen Klassen-
stufe, weshalb ein entsprechender Wissensstand zur
Benutzung des Programms angeraten ist.

Berechnungen, die das Lösen von Gleichungen oder
Gleichungssystemen erfordern, unterstützt Vector
Planez leider nicht, da hierfür ein CAS nötig ist.
 Warnung Was soll Ebene E schneiden? Werkzeuge Zum Ändern der Sprache muss das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden. Wollen Sie das jetzt tun? Zurücksetzen Zurücksetzen und Sprache ändern nicht orthogonal nicht parallel orthogonal parallel tmainform.bitbtncheckpunkt.caption&Prüfen tmainform.bitbtnvektorcheckok.caption&Prüfen tmainform.statictxtergebnispunktcheck.captionundefiniert tmainform.statictxtergebnisvektorcheck1.captionundefiniert tmainform.statictxtgleichungsform.captionParameterform tmainform.statictxtgleichungsform1.captionNormalenform tmainform.statictxtgleichungsform2.captionKoordinatenform tmainform.statictxtgrauwarn.captionSpeichere zunächst unter "Eingabe" eine Ebene! tmainform.statictxtgrauwarnorth.captionSpeichere zunächst unter "Eingabe" eine Ebene! tmainform.tabsheeteingabeparamform.captionParameterform tmainform.tabsheetkoordinatenform.captionKoordinatenform tmainform.tabsheetnormalenform.captionNormalenform umain.i18nerrorFehler umain.i18nzerovectorwarnWie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach... utranslationstrings.i18nerrorFehler utranslationstrings.i18nsaveplanefirstSpeichere zunächst unter "Eingabe" eine Ebene! utranslationstrings.i18nundefinedundefiniert utranslationstrings.i18nzerovectorwarnWie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach... Über Vector Planez Content-Type: text/plain; charset=UTF-8
Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
  wird nicht unterstützt. Bitte öffne nur .planez-Dateien! &Bearbeiten &Berechnen &Datei &Dokumentation &Ebene speichern &Hilfe &OK &Schnittwinkel berechnen &Speichern unter &Öffnen &Über Vector Planez (dient als 2. Ebene bei diversen Berechnungen) * Bedingung: E ∥ F bzw. E ∥ g Abbrechen Abstand Abstand berechnen Abstand von Ebene E und Alles &zurücksetzen Alles zurücksetzen Alles zurücksetzen und Sprache ändern Als &Sekundärebene F speichern Beachte auch, dass Dateien aus dem Internet oder von Fremden ungültig sein oder Schadcode enthalten können. Bedingung: E ∦ F bzw. E ∦ g Behalten Bestimme hier den Abstand zwischen deiner Ebene E
und einer zweiten Ebene F, einer Gerade g oder einem Punkt Q.
 Bestimme hier den Schnittwinkel zwischen deiner Ebene E
und einer zweiten Ebene F oder einer Gerade i.
 Bestätigen Compiler: Datei erfolgreich gespeichert Datei ersetzen Datei existiert nicht Der Dateityp  Deutsch Du hast das Speichern abgebrochen Du hast keine Datei ausgewählt! Ebene erfolgreich geladen! Ebene erfolgreich gespeichert! Ebene ersetzen Ebene laden Ebenen-Datei speichern unter Ebenen-Datei öffnen Ebenen-Rechner Eingabe Englisch Entwickler: Erfolg Ergebnis:  Ersetzen Gerade g * Gerade i Gespeichert Gib hier deine Ebenengleichung in 
Parameter-, Normalen- oder Koordinatenform ein:
 Gib zunächst eine Ebene ein! Info Kreuzprodukt Lagebeziehung Lizenz: Lotfußpunkt: Mindestens ein Vektor ist ungültig, da er weder Betrag noch Richtung hat. Normaleneinheitsvektor Normalenvektor Orthogonalität Programm zum Rechnen mit Ebenen Programmfehler &melden Programmier-
sprache: 
 Prüfe hier, ob ein Punkt P auf der Ebene E liegt (diese in "Allgemein" speichern). 
Dazu einfach die Koordinaten unten eintragen und auf "Prüfen" klicken.
 Punkt Q Punktprobe Quellcode: Schnittwinkel Sekundärebene F Sekundärebene F * Soll das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden? Soll die bestehende Datei ersetzt werden? Soll die bestehende Ebene ersetzt werden? Speichere zunächst unter "Eingabe" eine 
Ebene E (und ggf. eine Sekundärebene F)!
 Speichere zunächst unter "Eingabe" eine Ebene E
(und ggf. eine Sekundärebene F)!
 Speichere zunächst unter "Eingabe" eine Ebene E (und ggf. eine Sekundärebene F)! Spiegelung des Punkts an der Ebene: Sprache Stelle hier die Gleichung einer Gerade h auf, welche die in "Allgemein" 
festgelegte Ebene E senkrecht schneidet. Gib zunächst den Schnittpunkt ein.
 Stützvektor nehmen Umformung Ungültige Datei Ungültige JSON-Datei! Öffne ausschließlich unveränderte, direkt von Vector Planez generierte .planez-Dateien. Ungültiger Vektor Ungültiges Dateiformat Vector Planez Vector Planez hat deine Ebenengleichung 
in folgende Formen umgewandelt:
 Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Ebene F ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich nicht parallel zur Geraden g ist, sondern diese schneidet. In diesem Fall ist die Abstandsangabe inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Ebene F ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt. Vector Planez hat festgestellt, dass deine Ebene E vermutlich parallel zur Geraden i ist, weshalb kein Schnittwinkel existiert. In diesem Fall ist die Ausgabe somit inkorrekt. Vector Planez ist ein quelloffenes Programm zum 
Rechnen mit Ebenen aus der Mathematik. Diese
finden sich im Lehrplan der 12. gymnasialen Klassen-
stufe, weshalb ein entsprechender Wissensstand zur
Benutzung des Programms angeraten ist.

Berechnungen, die das Lösen von Gleichungen oder
Gleichungssystemen erfordern, unterstützt Vector
Planez leider nicht, da hierfür ein CAS nötig ist.
 Warnung Was soll Ebene E schneiden? Werkzeuge Zum Ändern der Sprache muss das gesamte Programm inklusive aller Ein- und Ausgaben zurückgesetzt werden. Wollen Sie das jetzt tun? Zurücksetzen Zurücksetzen und Sprache ändern nicht orthogonal nicht parallel orthogonal parallel &Prüfen &Prüfen undefiniert undefiniert Parameterform Normalenform Koordinatenform Speichere zunächst unter "Eingabe" eine Ebene! Speichere zunächst unter "Eingabe" eine Ebene! Parameterform Koordinatenform Normalenform Fehler Wie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach... Fehler Speichere zunächst unter "Eingabe" eine Ebene! undefiniert Wie genau stellst du dir eine Ebene vor, die orthogonal zum Vektor (0,0,0) ist? Denk mal drüber nach... Über Vector Planez 